
//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: bspline_create.h
//
// MATLAB Coder version            : 5.4
//

#ifndef BSPLINE_CREATE_H
#define BSPLINE_CREATE_H

// Include Files
#include "opencn_matlab_types2.h"
#include "rtwtypes.h"
#include <cstddef>
#include <cstdlib>

// Custom Header Code
#include "functions.h"
// Function Declarations
namespace ocn {
extern void bspline_create(int degree, const double breakpoints[10], BaseSplineStruct *Bl);

}

#endif
//
// File trailer for bspline_create.h
//
// [EOF]
//
