
//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: constrAxesStructType.h
//
// MATLAB Coder version            : 5.4
//

#ifndef CONSTRAXESSTRUCTTYPE_H
#define CONSTRAXESSTRUCTTYPE_H

// Include Files
#include "opencn_matlab_types3.h"
#include "rtwtypes.h"
#include <cstddef>
#include <cstdlib>

// Custom Header Code
#include "functions.h"
// Function Declarations
namespace ocn {
extern void constrAxesStructType(double a__1, Axes *C);

}

#endif
//
// File trailer for constrAxesStructType.h
//
// [EOF]
//
