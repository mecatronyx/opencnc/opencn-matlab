
//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: kin_xyzbc_tt_inverse.cpp
//
// MATLAB Coder version            : 5.4
//

// Include Files
#include "kin_xyzbc_tt_inverse.h"
#include "coder_array.h"
#include <cmath>

// Function Definitions
//
// function joint = kin_xyzbc_tt_inverse(in1,in2)
//
// KIN_XYZBC_TT_INVERSE
//     JOINT = KIN_XYZBC_TT_INVERSE(IN1,IN2)
//
// Arguments    : const ::coder::array<double, 1U> &in1
//                const ::coder::array<double, 1U> &in2
//                double joint[5]
// Return Type  : void
//
namespace ocn {
void kin_xyzbc_tt_inverse(const ::coder::array<double, 1U> &in1,
                          const ::coder::array<double, 1U> &in2, double joint[5])
{
    double t2;
    double t3;
    double t4;
    double t5;
    double t6;
    double t7;
    double t8;
    //     This function was generated by the Symbolic Math Toolbox version 9.1.
    //     07-Nov-2023 14:54:32
    // Inverse Kinematics :
    // INPUTS :
    // 	r_t    (5x1) : pose vector ( piece frame )
    // 	p      (3x5) : parameters
    // OUTPUTS :
    // 	r_j    (5x1) : pose vector( joint space )
    // 'kin_xyzbc_tt_inverse:15' b = in1(4,:);
    // 'kin_xyzbc_tt_inverse:16' c = in1(5,:);
    // 'kin_xyzbc_tt_inverse:17' offB1 = in2(10);
    // 'kin_xyzbc_tt_inverse:18' offC1 = in2(13);
    // 'kin_xyzbc_tt_inverse:19' offB3 = in2(12);
    // 'kin_xyzbc_tt_inverse:20' offC2 = in2(14);
    // 'kin_xyzbc_tt_inverse:21' offC3 = in2(15);
    // 'kin_xyzbc_tt_inverse:22' offM1 = in2(1);
    // 'kin_xyzbc_tt_inverse:23' offM2 = in2(2);
    // 'kin_xyzbc_tt_inverse:24' offM3 = in2(3);
    // 'kin_xyzbc_tt_inverse:25' offP1 = in2(7);
    // 'kin_xyzbc_tt_inverse:26' offP2 = in2(8);
    // 'kin_xyzbc_tt_inverse:27' offP3 = in2(9);
    // 'kin_xyzbc_tt_inverse:28' offT1 = in2(4);
    // 'kin_xyzbc_tt_inverse:29' offT2 = in2(5);
    // 'kin_xyzbc_tt_inverse:30' offT3 = in2(6);
    // 'kin_xyzbc_tt_inverse:31' x = in1(1,:);
    // 'kin_xyzbc_tt_inverse:32' y = in1(2,:);
    // 'kin_xyzbc_tt_inverse:33' z = in1(3,:);
    // 'kin_xyzbc_tt_inverse:34' t2 = cos(b);
    t2 = std::cos(in1[3]);
    // 'kin_xyzbc_tt_inverse:35' t3 = cos(c);
    t3 = std::cos(in1[4]);
    // 'kin_xyzbc_tt_inverse:36' t4 = sin(b);
    t4 = std::sin(in1[3]);
    // 'kin_xyzbc_tt_inverse:37' t5 = sin(c);
    t5 = std::sin(in1[4]);
    // 'kin_xyzbc_tt_inverse:38' t6 = offP1+x;
    t6 = in1[0] + in2[6];
    // 'kin_xyzbc_tt_inverse:39' t7 = offP2+y;
    t7 = in1[1] + in2[7];
    // 'kin_xyzbc_tt_inverse:40' t8 = offP3+z;
    t8 = in1[2] + in2[8];
    // 'kin_xyzbc_tt_inverse:41' joint =
    // [-offB1-offC1+offM1-offT1+offB1.*t2+offB3.*t4+offC3.*t4+t4.*t8+offC1.*t2.*t3-offC2.*t2.*t5+t2.*t3.*t6-t2.*t5.*t7;-offC2+offM2-offT2+offC2.*t3+offC1.*t5+t3.*t7+t5.*t6;-offB3-offC3+offM3-offT3-offB1.*t4+offB3.*t2+offC3.*t2+t2.*t8-offC1.*t3.*t4+offC2.*t4.*t5-t3.*t4.*t6+t4.*t5.*t7;b;c];
    joint[0] = ((((((((((-in2[9] - in2[12]) + in2[0]) - in2[3]) + in2[9] * t2) + in2[11] * t4) +
                    in2[14] * t4) +
                   t4 * t8) +
                  in2[12] * t2 * t3) -
                 in2[13] * t2 * t5) +
                t2 * t3 * t6) -
               t2 * t5 * t7;
    joint[1] =
        (((((-in2[13] + in2[1]) - in2[4]) + in2[13] * t3) + in2[12] * t5) + t3 * t7) + t5 * t6;
    joint[2] = ((((((((((-in2[11] - in2[14]) + in2[2]) - in2[5]) - in2[9] * t4) + in2[11] * t2) +
                    in2[14] * t2) +
                   t2 * t8) -
                  in2[12] * t3 * t4) +
                 in2[13] * t4 * t5) -
                t3 * t4 * t6) +
               t4 * t5 * t7;
    joint[3] = in1[3];
    joint[4] = in1[4];
}

} // namespace ocn

//
// File trailer for kin_xyzbc_tt_inverse.cpp
//
// [EOF]
//
