
//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: opencn_matlab_terminate.cpp
//
// MATLAB Coder version            : 5.4
//

// Include Files
#include "opencn_matlab_terminate.h"
#include "opencn_matlab_data.h"

// Function Definitions
//
// Arguments    : void
// Return Type  : void
//
namespace ocn {
void opencn_matlab_terminate()
{
    isInitialized_opencn_matlab = false;
}

} // namespace ocn

//
// File trailer for opencn_matlab_terminate.cpp
//
// [EOF]
//
