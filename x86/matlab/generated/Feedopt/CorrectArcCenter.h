
//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: CorrectArcCenter.h
//
// MATLAB Coder version            : 5.4
//

#ifndef CORRECTARCCENTER_H
#define CORRECTARCCENTER_H

// Include Files
#include "rtwtypes.h"
#include <cstddef>
#include <cstdlib>

// Custom Header Code
#include "functions.h"
// Function Declarations
namespace ocn {
void CorrectArcCenter(const double P0[2], const double P1[2], double C[2], double *R,
                      double *delta);

}

#endif
//
// File trailer for CorrectArcCenter.h
//
// [EOF]
//
