
//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: FeedoptPlan.h
//
// MATLAB Coder version            : 5.4
//

#ifndef FEEDOPTPLAN_H
#define FEEDOPTPLAN_H

// Include Files
#include "opencn_matlab_types.h"
#include "opencn_matlab_types111.h"
#include "rtwtypes.h"
#include <cstddef>
#include <cstdlib>

// Custom Header Code
#include "functions.h"
// Function Declarations
namespace ocn {
extern void FeedoptPlan(FeedoptContext *ctx, bool *optimized, CurvStruct *opt_struct);

}

#endif
//
// File trailer for FeedoptPlan.h
//
// [EOF]
//
