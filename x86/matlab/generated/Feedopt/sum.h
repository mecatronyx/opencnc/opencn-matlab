
//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: sum.h
//
// MATLAB Coder version            : 5.4
//

#ifndef SUM_H
#define SUM_H

// Include Files
#include "rtwtypes.h"
#include "coder_array.h"
#include <cstddef>
#include <cstdlib>

// Custom Header Code
#include "functions.h"
// Function Declarations
namespace ocn {
namespace coder {
void sum(const ::coder::array<double, 2U> &x, ::coder::array<double, 2U> &y);

void sum(const ::coder::array<double, 2U> &x, double y[5]);

double sum(const ::coder::array<double, 2U> &x);

double sum(const ::coder::array<double, 1U> &x);

} // namespace coder
} // namespace ocn

#endif
//
// File trailer for sum.h
//
// [EOF]
//
