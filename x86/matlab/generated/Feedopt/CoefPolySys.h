
//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: CoefPolySys.h
//
// MATLAB Coder version            : 5.4
//

#ifndef COEFPOLYSYS_H
#define COEFPOLYSYS_H

// Include Files
#include "rtwtypes.h"
#include <cstddef>
#include <cstdlib>

// Custom Header Code
#include "functions.h"
// Function Declarations
namespace ocn {
void CoefPolySys(const double in1[6], const double in2[6], const double in3[6], const double in4[6],
                 const double in5[6], const double in6[6], const double in7[6], double CoefPS[16]);

}

#endif
//
// File trailer for CoefPolySys.h
//
// [EOF]
//
