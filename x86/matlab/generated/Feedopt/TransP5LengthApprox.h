
//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: TransP5LengthApprox.h
//
// MATLAB Coder version            : 5.4
//

#ifndef TRANSP5LENGTHAPPROX_H
#define TRANSP5LENGTHAPPROX_H

// Include Files
#include "rtwtypes.h"
#include "coder_array.h"
#include <cstddef>
#include <cstdlib>

// Custom Header Code
#include "functions.h"
// Function Declarations
namespace ocn {
double TransP5LengthApprox(const ::coder::array<double, 2U> &CurvStruct_CoeffP5, double u0,
                           double u1);

}

#endif
//
// File trailer for TransP5LengthApprox.h
//
// [EOF]
//
