
//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: CheckCurvStructs.h
//
// MATLAB Coder version            : 5.4
//

#ifndef CHECKCURVSTRUCTS_H
#define CHECKCURVSTRUCTS_H

// Include Files
#include "rtwtypes.h"
#include <cstddef>
#include <cstdlib>

// Custom Header Code
#include "functions.h"
// Type Declarations
namespace ocn {
struct b_FeedoptContext;

}

// Function Declarations
namespace ocn {
void CheckCurvStructs(b_FeedoptContext *ctx);

}

#endif
//
// File trailer for CheckCurvStructs.h
//
// [EOF]
//
