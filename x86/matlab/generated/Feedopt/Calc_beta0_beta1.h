
//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: Calc_beta0_beta1.h
//
// MATLAB Coder version            : 5.4
//

#ifndef CALC_BETA0_BETA1_H
#define CALC_BETA0_BETA1_H

// Include Files
#include "rtwtypes.h"
#include <cstddef>
#include <cstdlib>

// Custom Header Code
#include "functions.h"
// Function Declarations
namespace ocn {
void Calc_beta0_beta1(double alpha0, double alpha1, const double in3[6], const double in4[6],
                      const double in5[6], const double in6[6], const double in7[6],
                      const double in8[6], const double in9[6], double *beta0, double *beta1);

}

#endif
//
// File trailer for Calc_beta0_beta1.h
//
// [EOF]
//
