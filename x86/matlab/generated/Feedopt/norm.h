
//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: norm.h
//
// MATLAB Coder version            : 5.4
//

#ifndef NORM_H
#define NORM_H

// Include Files
#include "rtwtypes.h"
#include "coder_array.h"
#include <cstddef>
#include <cstdlib>

// Custom Header Code
#include "functions.h"
// Function Declarations
namespace ocn {
namespace coder {
double b_norm(const ::coder::array<double, 1U> &x);

}
} // namespace ocn

#endif
//
// File trailer for norm.h
//
// [EOF]
//
