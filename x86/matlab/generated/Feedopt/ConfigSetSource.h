
//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: ConfigSetSource.h
//
// MATLAB Coder version            : 5.4
//

#ifndef CONFIGSETSOURCE_H
#define CONFIGSETSOURCE_H

// Include Files
#include "opencn_matlab_types.h"
#include "rtwtypes.h"
#include <cstddef>
#include <cstdlib>

// Custom Header Code
#include "functions.h"
// Function Declarations
namespace ocn {
extern void ConfigSetSource(FeedoptConfig *cfg, const char filename_data[],
                            const int filename_size[2]);

}

#endif
//
// File trailer for ConfigSetSource.h
//
// [EOF]
//
