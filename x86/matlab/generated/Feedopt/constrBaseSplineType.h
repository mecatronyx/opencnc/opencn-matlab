
//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: constrBaseSplineType.h
//
// MATLAB Coder version            : 5.4
//

#ifndef CONSTRBASESPLINETYPE_H
#define CONSTRBASESPLINETYPE_H

// Include Files
#include "opencn_matlab_types2.h"
#include "rtwtypes.h"
#include <cstddef>
#include <cstdlib>

// Custom Header Code
#include "functions.h"
// Function Declarations
namespace ocn {
extern void constrBaseSplineType(double a__1, BaseSplineStruct *C);

}

#endif
//
// File trailer for constrBaseSplineType.h
//
// [EOF]
//
