
//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: EvalHelix.cpp
//
// MATLAB Coder version            : 5.4
//

// Include Files
#include "EvalHelix.h"
#include "opencn_matlab_data.h"
#include "coder_array.h"
#include "common/tracy/Tracy.hpp"
#include <cmath>
#include <emmintrin.h>

// Function Declarations
namespace ocn {
static void binary_expand_op(::coder::array<double, 2U> &in1, const ::coder::array<double, 2U> &in2,
                             const ::coder::array<double, 2U> &in3, double in4, const double in5[3],
                             const ::coder::array<double, 2U> &in6);

static void binary_expand_op(::coder::array<double, 2U> &in1, double in2,
                             const ::coder::array<double, 2U> &in3,
                             const ::coder::array<double, 2U> &in4);

static void binary_expand_op(::coder::array<double, 2U> &in1, double in2,
                             const ::coder::array<double, 2U> &in3, double in4,
                             const ::coder::array<double, 2U> &in5);

static void minus(double in1[3], const double in2_data[], const int *in2_size, const double in3[3]);

} // namespace ocn

// Function Definitions
//
// Arguments    : ::coder::array<double, 2U> &in1
//                const ::coder::array<double, 2U> &in2
//                const ::coder::array<double, 2U> &in3
//                double in4
//                const double in5[3]
//                const ::coder::array<double, 2U> &in6
// Return Type  : void
//
namespace ocn {
static void binary_expand_op(::coder::array<double, 2U> &in1, const ::coder::array<double, 2U> &in2,
                             const ::coder::array<double, 2U> &in3, double in4, const double in5[3],
                             const ::coder::array<double, 2U> &in6)
{
    ::coder::array<double, 2U> b_in4;
    int aux_0_1;
    int aux_1_1;
    int aux_2_1;
    int b_loop_ub;
    int i1;
    int loop_ub;
    int stride_0_1;
    int stride_1_1;
    int stride_2_1;
    b_in4.set_size(3, in6.size(1));
    loop_ub = in6.size(1);
    for (int i{0}; i < loop_ub; i++) {
        __m128d r;
        r = _mm_loadu_pd((const double *)&in5[0]);
        _mm_storeu_pd(&b_in4[3 * i],
                      _mm_mul_pd(_mm_mul_pd(_mm_set1_pd(in4), r), _mm_set1_pd(in6[i])));
        b_in4[3 * i + 2] = in4 * in5[2] * in6[i];
    }
    if (b_in4.size(1) == 1) {
        if (in3.size(1) == 1) {
            i1 = in2.size(1);
        } else {
            i1 = in3.size(1);
        }
    } else {
        i1 = b_in4.size(1);
    }
    in1.set_size(3, i1);
    stride_0_1 = (in2.size(1) != 1);
    stride_1_1 = (in3.size(1) != 1);
    stride_2_1 = (b_in4.size(1) != 1);
    aux_0_1 = 0;
    aux_1_1 = 0;
    aux_2_1 = 0;
    if (b_in4.size(1) == 1) {
        if (in3.size(1) == 1) {
            b_loop_ub = in2.size(1);
        } else {
            b_loop_ub = in3.size(1);
        }
    } else {
        b_loop_ub = b_in4.size(1);
    }
    for (int i2{0}; i2 < b_loop_ub; i2++) {
        __m128d r1;
        __m128d r2;
        __m128d r3;
        r1 = _mm_loadu_pd((const double *)&in2[3 * aux_0_1]);
        r2 = _mm_loadu_pd((const double *)&in3[3 * aux_1_1]);
        r3 = _mm_loadu_pd(&b_in4[3 * aux_2_1]);
        _mm_storeu_pd(&in1[3 * i2], _mm_add_pd(_mm_add_pd(r1, r2), r3));
        in1[3 * i2 + 2] = (in2[3 * aux_0_1 + 2] + in3[3 * aux_1_1 + 2]) + b_in4[3 * aux_2_1 + 2];
        aux_2_1 += stride_2_1;
        aux_1_1 += stride_1_1;
        aux_0_1 += stride_0_1;
    }
}

//
// Arguments    : ::coder::array<double, 2U> &in1
//                double in2
//                const ::coder::array<double, 2U> &in3
//                const ::coder::array<double, 2U> &in4
// Return Type  : void
//
static void binary_expand_op(::coder::array<double, 2U> &in1, double in2,
                             const ::coder::array<double, 2U> &in3,
                             const ::coder::array<double, 2U> &in4)
{
    int aux_0_1;
    int aux_1_1;
    int i;
    int loop_ub;
    int stride_0_1;
    int stride_1_1;
    if (in4.size(1) == 1) {
        i = in3.size(1);
    } else {
        i = in4.size(1);
    }
    in1.set_size(3, i);
    stride_0_1 = (in3.size(1) != 1);
    stride_1_1 = (in4.size(1) != 1);
    aux_0_1 = 0;
    aux_1_1 = 0;
    if (in4.size(1) == 1) {
        loop_ub = in3.size(1);
    } else {
        loop_ub = in4.size(1);
    }
    for (int i1{0}; i1 < loop_ub; i1++) {
        __m128d r;
        __m128d r1;
        r = _mm_loadu_pd((const double *)&in3[3 * aux_0_1]);
        r1 = _mm_loadu_pd((const double *)&in4[3 * aux_1_1]);
        _mm_storeu_pd(&in1[3 * i1], _mm_add_pd(_mm_mul_pd(_mm_set1_pd(-in2), r),
                                               _mm_mul_pd(_mm_set1_pd(in2), r1)));
        in1[3 * i1 + 2] = -in2 * in3[3 * aux_0_1 + 2] + in2 * in4[3 * aux_1_1 + 2];
        aux_1_1 += stride_1_1;
        aux_0_1 += stride_0_1;
    }
}

//
// Arguments    : ::coder::array<double, 2U> &in1
//                double in2
//                const ::coder::array<double, 2U> &in3
//                double in4
//                const ::coder::array<double, 2U> &in5
// Return Type  : void
//
static void binary_expand_op(::coder::array<double, 2U> &in1, double in2,
                             const ::coder::array<double, 2U> &in3, double in4,
                             const ::coder::array<double, 2U> &in5)
{
    int aux_0_1;
    int aux_1_1;
    int i;
    int loop_ub;
    int stride_0_1;
    int stride_1_1;
    if (in5.size(1) == 1) {
        i = in3.size(1);
    } else {
        i = in5.size(1);
    }
    in1.set_size(3, i);
    stride_0_1 = (in3.size(1) != 1);
    stride_1_1 = (in5.size(1) != 1);
    aux_0_1 = 0;
    aux_1_1 = 0;
    if (in5.size(1) == 1) {
        loop_ub = in3.size(1);
    } else {
        loop_ub = in5.size(1);
    }
    for (int i1{0}; i1 < loop_ub; i1++) {
        __m128d r;
        __m128d r1;
        r = _mm_loadu_pd((const double *)&in3[3 * aux_0_1]);
        r1 = _mm_loadu_pd((const double *)&in5[3 * aux_1_1]);
        _mm_storeu_pd(&in1[3 * i1], _mm_sub_pd(_mm_mul_pd(_mm_set1_pd(in2), r),
                                               _mm_mul_pd(_mm_set1_pd(in4), r1)));
        in1[3 * i1 + 2] = in2 * in3[3 * aux_0_1 + 2] - in4 * in5[3 * aux_1_1 + 2];
        aux_1_1 += stride_1_1;
        aux_0_1 += stride_0_1;
    }
}

//
// Arguments    : double in1[3]
//                const double in2_data[]
//                const int *in2_size
//                const double in3[3]
// Return Type  : void
//
static void minus(double in1[3], const double in2_data[], const int *in2_size, const double in3[3])
{
    int stride_0_0;
    stride_0_0 = (*in2_size != 1);
    in1[0] = in2_data[0] - in3[0];
    in1[1] = in2_data[stride_0_0] - in3[1];
    in1[2] = in2_data[stride_0_0 << 1] - in3[2];
}

//
// function [ r0D, r1D, r2D, r3D ] = EvalHelix( CurvStruct, u_vec, maskCart )
//
// EvalHelix : Evalue the helix curv and its corresponding parametric
//  derivatives. The evaluation occurs on the specified points in the u
//  vector.
//
//  Inputs :
//  CurvStruct    : A struct filled the parameters correspondin to a Helix
//  u_vec         : A vector of specifided points for the evaluation of the
//                  curve
//  maskCart      : Carthesian mask. It indicates which index should be
//                considerate as carthesian one.
//
//  Outputs :
//  r0D           : The evaluated helix at the specified points
//  r1D           : The 1rst order parametric derivative of the curve at the
//                  specified points
//  r2D           : The 2nd order parametric derivative of the curve at the
//                  specified points
//  r3D           : The 3rd order parametric derivative of the curve at the
//                  specified points
//
// Arguments    : const ::coder::array<double, 1U> &CurvStruct_R0
//                const double CurvStruct_CorrectedHelixCenter[3]
//                const double CurvStruct_evec[3]
//                double CurvStruct_theta
//                double CurvStruct_pitch
//                const ::coder::array<double, 1U> &u_vec
//                const bool maskCart_data[]
//                const int maskCart_size[2]
//                double r0D[3]
//                double r1D[3]
//                double r2D[3]
//                double r3D[3]
// Return Type  : void
//
void EvalHelix(const ::coder::array<double, 1U> &CurvStruct_R0,
               const double CurvStruct_CorrectedHelixCenter[3], const double CurvStruct_evec[3],
               double CurvStruct_theta, double CurvStruct_pitch,
               const ::coder::array<double, 1U> &u_vec, const bool maskCart_data[],
               const int maskCart_size[2], double r0D[3], double r1D[3], double r2D[3],
               double r3D[3])
{
    ::coder::array<double, 1U> cphi;
    ::coder::array<double, 1U> phi_vec;
    ::coder::array<double, 1U> sphi;
    double P0_data[6];
    double CP0[3];
    double EcrCP0[3];
    int b_loop_ub;
    int c_loop_ub;
    int end;
    int i3;
    int i5;
    int loop_ub;
    int partialTrueCount;
    int scalarLB;
    int trueCount;
    int vectorUB;
    signed char tmp_data[6];
    // 'EvalHelix:22' if ~coder.target('MATLAB')
    // 'EvalHelix:23' coder.cinclude('common/tracy/Tracy.hpp');
    // 'EvalHelix:24' coder.inline('never')
    // 'EvalHelix:25' coder.ceval('ZoneScopedN', coder.opaque('const char*', '"EvalHelix"'));
    ZoneScopedN("EvalHelix");
    //  Extract parameters from the struct
    // 'EvalHelix:28' P0      = CurvStruct.R0( maskCart );
    end = maskCart_size[1] - 1;
    trueCount = 0;
    partialTrueCount = 0;
    for (int i{0}; i <= end; i++) {
        if (maskCart_data[i]) {
            trueCount++;
            tmp_data[partialTrueCount] = static_cast<signed char>(i + 1);
            partialTrueCount++;
        }
    }
    for (int b_i{0}; b_i < trueCount; b_i++) {
        P0_data[b_i] = CurvStruct_R0[tmp_data[b_i] - 1];
    }
    // 'EvalHelix:29' P1      = CurvStruct.R1( maskCart );
    // 'EvalHelix:30' evec    = CurvStruct.evec;
    // 'EvalHelix:31' theta   = CurvStruct.theta;
    // 'EvalHelix:32' pitch   = CurvStruct.pitch;
    // 'EvalHelix:34' P0P1    = P0 - P1;
    //
    // 'EvalHelix:37' C           = CurvStruct.CorrectedHelixCenter;
    // 'EvalHelix:38' CP0         = P0 - C;
    if (trueCount == 3) {
        __m128d r;
        r = _mm_loadu_pd(&P0_data[0]);
        _mm_storeu_pd(
            &CP0[0],
            _mm_sub_pd(r, _mm_loadu_pd((const double *)&CurvStruct_CorrectedHelixCenter[0])));
        CP0[2] = P0_data[2] - CurvStruct_CorrectedHelixCenter[2];
    } else {
        minus(CP0, P0_data, &trueCount, CurvStruct_CorrectedHelixCenter);
    }
    // 'EvalHelix:39' phi_vec     = theta * u_vec;
    phi_vec.set_size(u_vec.size(0));
    loop_ub = u_vec.size(0);
    scalarLB = (u_vec.size(0) / 2) << 1;
    vectorUB = scalarLB - 2;
    for (int i1{0}; i1 <= vectorUB; i1 += 2) {
        _mm_storeu_pd(&phi_vec[i1], _mm_mul_pd(_mm_set1_pd(CurvStruct_theta),
                                               _mm_loadu_pd((const double *)&u_vec[i1])));
    }
    for (int i1{scalarLB}; i1 < loop_ub; i1++) {
        phi_vec[i1] = CurvStruct_theta * u_vec[i1];
    }
    // 'EvalHelix:40' EcrCP0      = cross( evec, CP0 );
    EcrCP0[0] = CurvStruct_evec[1] * CP0[2] - CP0[1] * CurvStruct_evec[2];
    EcrCP0[1] = CP0[0] * CurvStruct_evec[2] - CurvStruct_evec[0] * CP0[2];
    EcrCP0[2] = CurvStruct_evec[0] * CP0[1] - CP0[0] * CurvStruct_evec[1];
    //  clockwise tangent vector
    // 'EvalHelix:41' cphi        = mycos( phi_vec );
    //  mycos : My implementation of cosinus.
    //
    //  Inputs :
    //    x   : The value use to evaluate the cosinus.
    //  Outputs :
    //    y   : The resulting value of the cosinus.
    //
    // 'mycos:10' y = cos( x );
    cphi.set_size(phi_vec.size(0));
    b_loop_ub = phi_vec.size(0);
    for (int i2{0}; i2 < b_loop_ub; i2++) {
        cphi[i2] = phi_vec[i2];
    }
    i3 = phi_vec.size(0);
    for (int k{0}; k < i3; k++) {
        cphi[k] = std::cos(cphi[k]);
    }
    // 'mycos:11' cos_calls = cos_calls + 1;
    cos_calls++;
    // 'EvalHelix:42' sphi        = mysin( phi_vec );
    //  mysin : Custom implementation of the sinus.
    //
    //  Inputs :
    //  Inputs values of the function.
    //
    //  Outputs :
    //  Resulting values.
    //
    // 'mysin:11' y = sin( x );
    sphi.set_size(phi_vec.size(0));
    c_loop_ub = phi_vec.size(0);
    for (int i4{0}; i4 < c_loop_ub; i4++) {
        sphi[i4] = phi_vec[i4];
    }
    i5 = phi_vec.size(0);
    for (int b_k{0}; b_k < i5; b_k++) {
        sphi[b_k] = std::sin(sphi[b_k]);
    }
    __m128d r1;
    __m128d r10;
    __m128d r2;
    __m128d r3;
    __m128d r4;
    __m128d r5;
    __m128d r6;
    __m128d r7;
    __m128d r8;
    __m128d r9;
    double a;
    double a_tmp;
    double b_a;
    double b_a_tmp;
    double d;
    double d1;
    double d2;
    double d3;
    // 'mysin:12' sin_calls = sin_calls + 1;
    sin_calls++;
    //
    // 'EvalHelix:45' cphiTCP0    = CP0 * cphi;
    // 'EvalHelix:46' sphiTCP0    = CP0 * sphi;
    // 'EvalHelix:47' cphiTEcrCP0 = EcrCP0 * cphi;
    // 'EvalHelix:48' sphiTEcrCP0 = EcrCP0 * sphi;
    //
    // 'EvalHelix:51' r0D       = bsxfun(@plus, C, cphiTCP0  + sphiTEcrCP0  + ...
    // 'EvalHelix:52'                    pitch/(2*pi)*evec*phi_vec);
    a = CurvStruct_pitch / 6.2831853071795862;
    // 'EvalHelix:53' r1D       = bsxfun(@plus, -theta  *sphiTCP0  + theta  *cphiTEcrCP0, ...
    // 'EvalHelix:54'                    theta * pitch/(2*pi) * evec);
    b_a = CurvStruct_theta * CurvStruct_pitch / 6.2831853071795862;
    // 'EvalHelix:55' r2D       = -theta^2*cphiTCP0  - theta^2*sphiTEcrCP0;
    a_tmp = CurvStruct_theta * CurvStruct_theta;
    // 'EvalHelix:56' r3D       =  theta^3*sphiTCP0  - theta^3*cphiTEcrCP0;
    b_a_tmp = std::pow(CurvStruct_theta, 3.0);
    r1 = _mm_loadu_pd(&CP0[0]);
    r3 = _mm_set1_pd(cphi[0]);
    r2 = _mm_mul_pd(r1, r3);
    r5 = _mm_set1_pd(sphi[0]);
    r4 = _mm_mul_pd(r1, r5);
    r6 = _mm_loadu_pd(&EcrCP0[0]);
    r7 = _mm_mul_pd(r6, r3);
    r8 = _mm_mul_pd(r6, r5);
    r9 = _mm_loadu_pd((const double *)&CurvStruct_evec[0]);
    _mm_storeu_pd(
        &r0D[0],
        _mm_add_pd(_mm_loadu_pd((const double *)&CurvStruct_CorrectedHelixCenter[0]),
                   _mm_add_pd(_mm_add_pd(r2, r8), _mm_mul_pd(_mm_mul_pd(_mm_set1_pd(a), r9),
                                                             _mm_set1_pd(phi_vec[0])))));
    _mm_storeu_pd(&r1D[0], _mm_add_pd(_mm_add_pd(_mm_mul_pd(_mm_set1_pd(-CurvStruct_theta), r4),
                                                 _mm_mul_pd(_mm_set1_pd(CurvStruct_theta), r7)),
                                      _mm_mul_pd(_mm_set1_pd(b_a), r9)));
    _mm_storeu_pd(&r2D[0], _mm_sub_pd(_mm_mul_pd(_mm_set1_pd(-a_tmp), r2),
                                      _mm_mul_pd(_mm_set1_pd(a_tmp), r8)));
    r10 = _mm_set1_pd(b_a_tmp);
    _mm_storeu_pd(&r3D[0], _mm_sub_pd(_mm_mul_pd(r10, r4), _mm_mul_pd(r10, r7)));
    d = cphi[0] * CP0[2];
    d1 = sphi[0] * CP0[2];
    d2 = cphi[0] * EcrCP0[2];
    d3 = sphi[0] * EcrCP0[2];
    r0D[2] = CurvStruct_CorrectedHelixCenter[2] + ((d + d3) + a * CurvStruct_evec[2] * phi_vec[0]);
    r1D[2] = (-CurvStruct_theta * d1 + CurvStruct_theta * d2) + b_a * CurvStruct_evec[2];
    r2D[2] = -a_tmp * d - a_tmp * d3;
    r3D[2] = b_a_tmp * d1 - b_a_tmp * d2;
}

//
// function [ r0D, r1D, r2D, r3D ] = EvalHelix( CurvStruct, u_vec, maskCart )
//
// EvalHelix : Evalue the helix curv and its corresponding parametric
//  derivatives. The evaluation occurs on the specified points in the u
//  vector.
//
//  Inputs :
//  CurvStruct    : A struct filled the parameters correspondin to a Helix
//  u_vec         : A vector of specifided points for the evaluation of the
//                  curve
//  maskCart      : Carthesian mask. It indicates which index should be
//                considerate as carthesian one.
//
//  Outputs :
//  r0D           : The evaluated helix at the specified points
//  r1D           : The 1rst order parametric derivative of the curve at the
//                  specified points
//  r2D           : The 2nd order parametric derivative of the curve at the
//                  specified points
//  r3D           : The 3rd order parametric derivative of the curve at the
//                  specified points
//
// Arguments    : const ::coder::array<double, 1U> &CurvStruct_R0
//                const double CurvStruct_CorrectedHelixCenter[3]
//                const double CurvStruct_evec[3]
//                double CurvStruct_theta
//                double CurvStruct_pitch
//                double u_vec
//                const bool maskCart_data[]
//                const int maskCart_size[2]
//                double r0D[3]
//                double r1D[3]
//                double r2D[3]
//                double r3D[3]
// Return Type  : void
//
void b_EvalHelix(const ::coder::array<double, 1U> &CurvStruct_R0,
                 const double CurvStruct_CorrectedHelixCenter[3], const double CurvStruct_evec[3],
                 double CurvStruct_theta, double CurvStruct_pitch, double u_vec,
                 const bool maskCart_data[], const int maskCart_size[2], double r0D[3],
                 double r1D[3], double r2D[3], double r3D[3])
{
    __m128d r1;
    __m128d r10;
    __m128d r2;
    __m128d r3;
    __m128d r4;
    __m128d r5;
    __m128d r6;
    __m128d r7;
    __m128d r8;
    __m128d r9;
    double P0_data[6];
    double CP0[3];
    double EcrCP0[3];
    double a;
    double a_tmp;
    double b_a;
    double b_a_tmp;
    double cphi;
    double d;
    double d1;
    double d2;
    double d3;
    double phi_vec;
    double sphi;
    int end;
    int partialTrueCount;
    int trueCount;
    signed char tmp_data[6];
    // 'EvalHelix:22' if ~coder.target('MATLAB')
    // 'EvalHelix:23' coder.cinclude('common/tracy/Tracy.hpp');
    // 'EvalHelix:24' coder.inline('never')
    // 'EvalHelix:25' coder.ceval('ZoneScopedN', coder.opaque('const char*', '"EvalHelix"'));
    ZoneScopedN("EvalHelix");
    //  Extract parameters from the struct
    // 'EvalHelix:28' P0      = CurvStruct.R0( maskCart );
    end = maskCart_size[1] - 1;
    trueCount = 0;
    partialTrueCount = 0;
    for (int i{0}; i <= end; i++) {
        if (maskCart_data[i]) {
            trueCount++;
            tmp_data[partialTrueCount] = static_cast<signed char>(i + 1);
            partialTrueCount++;
        }
    }
    for (int b_i{0}; b_i < trueCount; b_i++) {
        P0_data[b_i] = CurvStruct_R0[tmp_data[b_i] - 1];
    }
    // 'EvalHelix:29' P1      = CurvStruct.R1( maskCart );
    // 'EvalHelix:30' evec    = CurvStruct.evec;
    // 'EvalHelix:31' theta   = CurvStruct.theta;
    // 'EvalHelix:32' pitch   = CurvStruct.pitch;
    // 'EvalHelix:34' P0P1    = P0 - P1;
    //
    // 'EvalHelix:37' C           = CurvStruct.CorrectedHelixCenter;
    // 'EvalHelix:38' CP0         = P0 - C;
    if (trueCount == 3) {
        __m128d r;
        r = _mm_loadu_pd(&P0_data[0]);
        _mm_storeu_pd(
            &CP0[0],
            _mm_sub_pd(r, _mm_loadu_pd((const double *)&CurvStruct_CorrectedHelixCenter[0])));
        CP0[2] = P0_data[2] - CurvStruct_CorrectedHelixCenter[2];
    } else {
        minus(CP0, P0_data, &trueCount, CurvStruct_CorrectedHelixCenter);
    }
    // 'EvalHelix:39' phi_vec     = theta * u_vec;
    phi_vec = CurvStruct_theta * u_vec;
    // 'EvalHelix:40' EcrCP0      = cross( evec, CP0 );
    EcrCP0[0] = CurvStruct_evec[1] * CP0[2] - CP0[1] * CurvStruct_evec[2];
    EcrCP0[1] = CP0[0] * CurvStruct_evec[2] - CurvStruct_evec[0] * CP0[2];
    EcrCP0[2] = CurvStruct_evec[0] * CP0[1] - CP0[0] * CurvStruct_evec[1];
    //  clockwise tangent vector
    // 'EvalHelix:41' cphi        = mycos( phi_vec );
    //  mycos : My implementation of cosinus.
    //
    //  Inputs :
    //    x   : The value use to evaluate the cosinus.
    //  Outputs :
    //    y   : The resulting value of the cosinus.
    //
    // 'mycos:10' y = cos( x );
    cphi = std::cos(phi_vec);
    // 'mycos:11' cos_calls = cos_calls + 1;
    cos_calls++;
    // 'EvalHelix:42' sphi        = mysin( phi_vec );
    //  mysin : Custom implementation of the sinus.
    //
    //  Inputs :
    //  Inputs values of the function.
    //
    //  Outputs :
    //  Resulting values.
    //
    // 'mysin:11' y = sin( x );
    sphi = std::sin(phi_vec);
    // 'mysin:12' sin_calls = sin_calls + 1;
    sin_calls++;
    //
    // 'EvalHelix:45' cphiTCP0    = CP0 * cphi;
    // 'EvalHelix:46' sphiTCP0    = CP0 * sphi;
    // 'EvalHelix:47' cphiTEcrCP0 = EcrCP0 * cphi;
    // 'EvalHelix:48' sphiTEcrCP0 = EcrCP0 * sphi;
    //
    // 'EvalHelix:51' r0D       = bsxfun(@plus, C, cphiTCP0  + sphiTEcrCP0  + ...
    // 'EvalHelix:52'                    pitch/(2*pi)*evec*phi_vec);
    a = CurvStruct_pitch / 6.2831853071795862;
    // 'EvalHelix:53' r1D       = bsxfun(@plus, -theta  *sphiTCP0  + theta  *cphiTEcrCP0, ...
    // 'EvalHelix:54'                    theta * pitch/(2*pi) * evec);
    b_a = CurvStruct_theta * CurvStruct_pitch / 6.2831853071795862;
    // 'EvalHelix:55' r2D       = -theta^2*cphiTCP0  - theta^2*sphiTEcrCP0;
    a_tmp = CurvStruct_theta * CurvStruct_theta;
    // 'EvalHelix:56' r3D       =  theta^3*sphiTCP0  - theta^3*cphiTEcrCP0;
    b_a_tmp = std::pow(CurvStruct_theta, 3.0);
    r1 = _mm_loadu_pd(&CP0[0]);
    r3 = _mm_set1_pd(cphi);
    r2 = _mm_mul_pd(r1, r3);
    r5 = _mm_set1_pd(sphi);
    r4 = _mm_mul_pd(r1, r5);
    r6 = _mm_loadu_pd(&EcrCP0[0]);
    r7 = _mm_mul_pd(r6, r3);
    r8 = _mm_mul_pd(r6, r5);
    r9 = _mm_loadu_pd((const double *)&CurvStruct_evec[0]);
    _mm_storeu_pd(
        &r0D[0],
        _mm_add_pd(_mm_loadu_pd((const double *)&CurvStruct_CorrectedHelixCenter[0]),
                   _mm_add_pd(_mm_add_pd(r2, r8),
                              _mm_mul_pd(_mm_mul_pd(_mm_set1_pd(a), r9), _mm_set1_pd(phi_vec)))));
    _mm_storeu_pd(&r1D[0], _mm_add_pd(_mm_add_pd(_mm_mul_pd(_mm_set1_pd(-CurvStruct_theta), r4),
                                                 _mm_mul_pd(_mm_set1_pd(CurvStruct_theta), r7)),
                                      _mm_mul_pd(_mm_set1_pd(b_a), r9)));
    _mm_storeu_pd(&r2D[0], _mm_sub_pd(_mm_mul_pd(_mm_set1_pd(-a_tmp), r2),
                                      _mm_mul_pd(_mm_set1_pd(a_tmp), r8)));
    r10 = _mm_set1_pd(b_a_tmp);
    _mm_storeu_pd(&r3D[0], _mm_sub_pd(_mm_mul_pd(r10, r4), _mm_mul_pd(r10, r7)));
    d = CP0[2] * cphi;
    d1 = CP0[2] * sphi;
    d2 = EcrCP0[2] * cphi;
    d3 = EcrCP0[2] * sphi;
    r0D[2] = CurvStruct_CorrectedHelixCenter[2] + ((d + d3) + a * CurvStruct_evec[2] * phi_vec);
    r1D[2] = (-CurvStruct_theta * d1 + CurvStruct_theta * d2) + b_a * CurvStruct_evec[2];
    r2D[2] = -a_tmp * d - a_tmp * d3;
    r3D[2] = b_a_tmp * d1 - b_a_tmp * d2;
}

//
// function [ r0D, r1D, r2D, r3D ] = EvalHelix( CurvStruct, u_vec, maskCart )
//
// EvalHelix : Evalue the helix curv and its corresponding parametric
//  derivatives. The evaluation occurs on the specified points in the u
//  vector.
//
//  Inputs :
//  CurvStruct    : A struct filled the parameters correspondin to a Helix
//  u_vec         : A vector of specifided points for the evaluation of the
//                  curve
//  maskCart      : Carthesian mask. It indicates which index should be
//                considerate as carthesian one.
//
//  Outputs :
//  r0D           : The evaluated helix at the specified points
//  r1D           : The 1rst order parametric derivative of the curve at the
//                  specified points
//  r2D           : The 2nd order parametric derivative of the curve at the
//                  specified points
//  r3D           : The 3rd order parametric derivative of the curve at the
//                  specified points
//
// Arguments    : const ::coder::array<double, 1U> &CurvStruct_R0
//                const double CurvStruct_CorrectedHelixCenter[3]
//                const double CurvStruct_evec[3]
//                double CurvStruct_theta
//                double CurvStruct_pitch
//                const ::coder::array<double, 2U> &u_vec
//                const bool maskCart_data[]
//                const int maskCart_size[2]
//                ::coder::array<double, 2U> &r0D
//                ::coder::array<double, 2U> &r1D
//                ::coder::array<double, 2U> &r2D
//                ::coder::array<double, 2U> &r3D
// Return Type  : void
//
void c_EvalHelix(const ::coder::array<double, 1U> &CurvStruct_R0,
                 const double CurvStruct_CorrectedHelixCenter[3], const double CurvStruct_evec[3],
                 double CurvStruct_theta, double CurvStruct_pitch,
                 const ::coder::array<double, 2U> &u_vec, const bool maskCart_data[],
                 const int maskCart_size[2], ::coder::array<double, 2U> &r0D,
                 ::coder::array<double, 2U> &r1D, ::coder::array<double, 2U> &r2D,
                 ::coder::array<double, 2U> &r3D)
{
    ::coder::array<double, 2U> b;
    ::coder::array<double, 2U> c_a;
    ::coder::array<double, 2U> cphi;
    ::coder::array<double, 2U> cphiTCP0;
    ::coder::array<double, 2U> cphiTEcrCP0;
    ::coder::array<double, 2U> phi_vec;
    ::coder::array<double, 2U> r5;
    ::coder::array<double, 2U> sphi;
    ::coder::array<double, 2U> sphiTCP0;
    ::coder::array<double, 2U> sphiTEcrCP0;
    double P0_data[6];
    double CP0[3];
    double EcrCP0[3];
    double y[3];
    double a;
    double a_tmp;
    double b_a;
    double b_a_tmp;
    int b_loop_ub;
    int c_loop_ub;
    int d_loop_ub;
    int e_loop_ub;
    int end;
    int f_loop_ub;
    int g_loop_ub;
    int h_loop_ub;
    int i11;
    int i3;
    int i5;
    int loop_ub;
    int partialTrueCount;
    int scalarLB;
    int trueCount;
    int vectorUB;
    signed char tmp_data[6];
    // 'EvalHelix:22' if ~coder.target('MATLAB')
    // 'EvalHelix:23' coder.cinclude('common/tracy/Tracy.hpp');
    // 'EvalHelix:24' coder.inline('never')
    // 'EvalHelix:25' coder.ceval('ZoneScopedN', coder.opaque('const char*', '"EvalHelix"'));
    ZoneScopedN("EvalHelix");
    //  Extract parameters from the struct
    // 'EvalHelix:28' P0      = CurvStruct.R0( maskCart );
    end = maskCart_size[1] - 1;
    trueCount = 0;
    partialTrueCount = 0;
    for (int i{0}; i <= end; i++) {
        if (maskCart_data[i]) {
            trueCount++;
            tmp_data[partialTrueCount] = static_cast<signed char>(i + 1);
            partialTrueCount++;
        }
    }
    for (int b_i{0}; b_i < trueCount; b_i++) {
        P0_data[b_i] = CurvStruct_R0[tmp_data[b_i] - 1];
    }
    // 'EvalHelix:29' P1      = CurvStruct.R1( maskCart );
    // 'EvalHelix:30' evec    = CurvStruct.evec;
    // 'EvalHelix:31' theta   = CurvStruct.theta;
    // 'EvalHelix:32' pitch   = CurvStruct.pitch;
    // 'EvalHelix:34' P0P1    = P0 - P1;
    //
    // 'EvalHelix:37' C           = CurvStruct.CorrectedHelixCenter;
    // 'EvalHelix:38' CP0         = P0 - C;
    if (trueCount == 3) {
        __m128d r;
        r = _mm_loadu_pd(&P0_data[0]);
        _mm_storeu_pd(
            &CP0[0],
            _mm_sub_pd(r, _mm_loadu_pd((const double *)&CurvStruct_CorrectedHelixCenter[0])));
        CP0[2] = P0_data[2] - CurvStruct_CorrectedHelixCenter[2];
    } else {
        minus(CP0, P0_data, &trueCount, CurvStruct_CorrectedHelixCenter);
    }
    // 'EvalHelix:39' phi_vec     = theta * u_vec;
    phi_vec.set_size(1, u_vec.size(1));
    loop_ub = u_vec.size(1);
    scalarLB = (u_vec.size(1) / 2) << 1;
    vectorUB = scalarLB - 2;
    for (int i1{0}; i1 <= vectorUB; i1 += 2) {
        _mm_storeu_pd(&phi_vec[i1], _mm_mul_pd(_mm_set1_pd(CurvStruct_theta),
                                               _mm_loadu_pd((const double *)&u_vec[i1])));
    }
    for (int i1{scalarLB}; i1 < loop_ub; i1++) {
        phi_vec[i1] = CurvStruct_theta * u_vec[i1];
    }
    // 'EvalHelix:40' EcrCP0      = cross( evec, CP0 );
    EcrCP0[0] = CurvStruct_evec[1] * CP0[2] - CP0[1] * CurvStruct_evec[2];
    EcrCP0[1] = CP0[0] * CurvStruct_evec[2] - CurvStruct_evec[0] * CP0[2];
    EcrCP0[2] = CurvStruct_evec[0] * CP0[1] - CP0[0] * CurvStruct_evec[1];
    //  clockwise tangent vector
    // 'EvalHelix:41' cphi        = mycos( phi_vec );
    //  mycos : My implementation of cosinus.
    //
    //  Inputs :
    //    x   : The value use to evaluate the cosinus.
    //  Outputs :
    //    y   : The resulting value of the cosinus.
    //
    // 'mycos:10' y = cos( x );
    cphi.set_size(1, phi_vec.size(1));
    b_loop_ub = phi_vec.size(1);
    for (int i2{0}; i2 < b_loop_ub; i2++) {
        cphi[i2] = phi_vec[i2];
    }
    i3 = phi_vec.size(1);
    for (int k{0}; k < i3; k++) {
        cphi[k] = std::cos(cphi[k]);
    }
    // 'mycos:11' cos_calls = cos_calls + 1;
    cos_calls++;
    // 'EvalHelix:42' sphi        = mysin( phi_vec );
    //  mysin : Custom implementation of the sinus.
    //
    //  Inputs :
    //  Inputs values of the function.
    //
    //  Outputs :
    //  Resulting values.
    //
    // 'mysin:11' y = sin( x );
    sphi.set_size(1, phi_vec.size(1));
    c_loop_ub = phi_vec.size(1);
    for (int i4{0}; i4 < c_loop_ub; i4++) {
        sphi[i4] = phi_vec[i4];
    }
    i5 = phi_vec.size(1);
    for (int b_k{0}; b_k < i5; b_k++) {
        sphi[b_k] = std::sin(sphi[b_k]);
    }
    // 'mysin:12' sin_calls = sin_calls + 1;
    sin_calls++;
    //
    // 'EvalHelix:45' cphiTCP0    = CP0 * cphi;
    cphiTCP0.set_size(3, cphi.size(1));
    d_loop_ub = cphi.size(1);
    for (int i6{0}; i6 < d_loop_ub; i6++) {
        __m128d r1;
        r1 = _mm_loadu_pd(&CP0[0]);
        _mm_storeu_pd(&cphiTCP0[3 * i6], _mm_mul_pd(r1, _mm_set1_pd(cphi[i6])));
        cphiTCP0[3 * i6 + 2] = CP0[2] * cphi[i6];
    }
    // 'EvalHelix:46' sphiTCP0    = CP0 * sphi;
    sphiTCP0.set_size(3, sphi.size(1));
    e_loop_ub = sphi.size(1);
    for (int i7{0}; i7 < e_loop_ub; i7++) {
        __m128d r2;
        r2 = _mm_loadu_pd(&CP0[0]);
        _mm_storeu_pd(&sphiTCP0[3 * i7], _mm_mul_pd(r2, _mm_set1_pd(sphi[i7])));
        sphiTCP0[3 * i7 + 2] = CP0[2] * sphi[i7];
    }
    // 'EvalHelix:47' cphiTEcrCP0 = EcrCP0 * cphi;
    cphiTEcrCP0.set_size(3, cphi.size(1));
    f_loop_ub = cphi.size(1);
    for (int i8{0}; i8 < f_loop_ub; i8++) {
        __m128d r3;
        r3 = _mm_loadu_pd(&EcrCP0[0]);
        _mm_storeu_pd(&cphiTEcrCP0[3 * i8], _mm_mul_pd(r3, _mm_set1_pd(cphi[i8])));
        cphiTEcrCP0[3 * i8 + 2] = EcrCP0[2] * cphi[i8];
    }
    // 'EvalHelix:48' sphiTEcrCP0 = EcrCP0 * sphi;
    sphiTEcrCP0.set_size(3, sphi.size(1));
    g_loop_ub = sphi.size(1);
    for (int i9{0}; i9 < g_loop_ub; i9++) {
        __m128d r4;
        r4 = _mm_loadu_pd(&EcrCP0[0]);
        _mm_storeu_pd(&sphiTEcrCP0[3 * i9], _mm_mul_pd(r4, _mm_set1_pd(sphi[i9])));
        sphiTEcrCP0[3 * i9 + 2] = EcrCP0[2] * sphi[i9];
    }
    //
    // 'EvalHelix:51' r0D       = bsxfun(@plus, C, cphiTCP0  + sphiTEcrCP0  + ...
    // 'EvalHelix:52'                    pitch/(2*pi)*evec*phi_vec);
    a = CurvStruct_pitch / 6.2831853071795862;
    r5.set_size(3, phi_vec.size(1));
    h_loop_ub = phi_vec.size(1);
    for (int i10{0}; i10 < h_loop_ub; i10++) {
        _mm_storeu_pd(&r5[3 * i10],
                      _mm_mul_pd(_mm_mul_pd(_mm_set1_pd(a),
                                            _mm_loadu_pd((const double *)&CurvStruct_evec[0])),
                                 _mm_set1_pd(phi_vec[i10])));
        r5[3 * i10 + 2] = a * CurvStruct_evec[2] * phi_vec[i10];
    }
    if (cphiTCP0.size(1) == 1) {
        i11 = sphiTEcrCP0.size(1);
    } else {
        i11 = cphiTCP0.size(1);
    }
    if ((cphiTCP0.size(1) == sphiTEcrCP0.size(1)) && (i11 == r5.size(1))) {
        int i_loop_ub;
        b.set_size(3, phi_vec.size(1));
        i_loop_ub = phi_vec.size(1);
        for (int i12{0}; i12 < i_loop_ub; i12++) {
            __m128d r6;
            __m128d r7;
            r6 = _mm_loadu_pd(&cphiTCP0[3 * i12]);
            r7 = _mm_loadu_pd(&sphiTEcrCP0[3 * i12]);
            _mm_storeu_pd(
                &b[3 * i12],
                _mm_add_pd(
                    _mm_add_pd(r6, r7),
                    _mm_add_pd(
                        _mm_set1_pd(0.0),
                        _mm_mul_pd(_mm_mul_pd(_mm_set1_pd(a),
                                              _mm_loadu_pd((const double *)&CurvStruct_evec[0])),
                                   _mm_set1_pd(phi_vec[i12])))));
            b[3 * i12 + 2] = (cphiTCP0[3 * i12 + 2] + sphiTEcrCP0[3 * i12 + 2]) +
                             a * CurvStruct_evec[2] * phi_vec[i12];
        }
    } else {
        binary_expand_op(b, cphiTCP0, sphiTEcrCP0, a, CurvStruct_evec, phi_vec);
    }
    r0D.set_size(3, b.size(1));
    if (b.size(1) != 0) {
        int bcoef;
        int i13;
        bcoef = (b.size(1) != 1);
        i13 = b.size(1) - 1;
        for (int c_k{0}; c_k <= i13; c_k++) {
            __m128d r8;
            int varargin_3;
            varargin_3 = bcoef * c_k;
            r8 = _mm_loadu_pd(&b[3 * varargin_3]);
            _mm_storeu_pd(
                &r0D[3 * c_k],
                _mm_add_pd(_mm_loadu_pd((const double *)&CurvStruct_CorrectedHelixCenter[0]), r8));
            r0D[3 * c_k + 2] = CurvStruct_CorrectedHelixCenter[2] + b[3 * varargin_3 + 2];
        }
    }
    // 'EvalHelix:53' r1D       = bsxfun(@plus, -theta  *sphiTCP0  + theta  *cphiTEcrCP0, ...
    // 'EvalHelix:54'                    theta * pitch/(2*pi) * evec);
    b_a = CurvStruct_theta * CurvStruct_pitch / 6.2831853071795862;
    _mm_storeu_pd(&y[0],
                  _mm_mul_pd(_mm_set1_pd(b_a), _mm_loadu_pd((const double *)&CurvStruct_evec[0])));
    y[2] = b_a * CurvStruct_evec[2];
    if (sphiTCP0.size(1) == cphiTEcrCP0.size(1)) {
        int j_loop_ub;
        c_a.set_size(3, sphiTCP0.size(1));
        j_loop_ub = sphiTCP0.size(1);
        for (int i14{0}; i14 < j_loop_ub; i14++) {
            __m128d r11;
            __m128d r9;
            r9 = _mm_loadu_pd(&sphiTCP0[3 * i14]);
            r11 = _mm_loadu_pd(&cphiTEcrCP0[3 * i14]);
            _mm_storeu_pd(&c_a[3 * i14],
                          _mm_add_pd(_mm_mul_pd(_mm_set1_pd(-CurvStruct_theta), r9),
                                     _mm_mul_pd(_mm_set1_pd(CurvStruct_theta), r11)));
            c_a[3 * i14 + 2] = -CurvStruct_theta * sphiTCP0[3 * i14 + 2] +
                               CurvStruct_theta * cphiTEcrCP0[3 * i14 + 2];
        }
    } else {
        binary_expand_op(c_a, CurvStruct_theta, sphiTCP0, cphiTEcrCP0);
    }
    r1D.set_size(3, c_a.size(1));
    if (c_a.size(1) != 0) {
        __m128d r10;
        int acoef;
        int i15;
        acoef = (c_a.size(1) != 1);
        i15 = c_a.size(1) - 1;
        r10 = _mm_loadu_pd(&y[0]);
        for (int d_k{0}; d_k <= i15; d_k++) {
            __m128d r12;
            int varargin_2;
            varargin_2 = acoef * d_k;
            r12 = _mm_loadu_pd(&c_a[3 * varargin_2]);
            _mm_storeu_pd(&r1D[3 * d_k], _mm_add_pd(r12, r10));
            r1D[3 * d_k + 2] = c_a[3 * varargin_2 + 2] + y[2];
        }
    }
    // 'EvalHelix:55' r2D       = -theta^2*cphiTCP0  - theta^2*sphiTEcrCP0;
    a_tmp = CurvStruct_theta * CurvStruct_theta;
    if (cphiTCP0.size(1) == sphiTEcrCP0.size(1)) {
        int k_loop_ub;
        r2D.set_size(3, cphiTCP0.size(1));
        k_loop_ub = cphiTCP0.size(1);
        for (int i16{0}; i16 < k_loop_ub; i16++) {
            __m128d r13;
            __m128d r14;
            r13 = _mm_loadu_pd(&cphiTCP0[3 * i16]);
            r14 = _mm_loadu_pd(&sphiTEcrCP0[3 * i16]);
            _mm_storeu_pd(&r2D[3 * i16], _mm_sub_pd(_mm_mul_pd(_mm_set1_pd(-a_tmp), r13),
                                                    _mm_mul_pd(_mm_set1_pd(a_tmp), r14)));
            r2D[3 * i16 + 2] = -a_tmp * cphiTCP0[3 * i16 + 2] - a_tmp * sphiTEcrCP0[3 * i16 + 2];
        }
    } else {
        binary_expand_op(r2D, -a_tmp, cphiTCP0, a_tmp, sphiTEcrCP0);
    }
    // 'EvalHelix:56' r3D       =  theta^3*sphiTCP0  - theta^3*cphiTEcrCP0;
    b_a_tmp = std::pow(CurvStruct_theta, 3.0);
    if (sphiTCP0.size(1) == cphiTEcrCP0.size(1)) {
        int l_loop_ub;
        r3D.set_size(3, sphiTCP0.size(1));
        l_loop_ub = sphiTCP0.size(1);
        for (int i17{0}; i17 < l_loop_ub; i17++) {
            __m128d r15;
            __m128d r16;
            __m128d r17;
            r15 = _mm_loadu_pd(&sphiTCP0[3 * i17]);
            r16 = _mm_loadu_pd(&cphiTEcrCP0[3 * i17]);
            r17 = _mm_set1_pd(b_a_tmp);
            _mm_storeu_pd(&r3D[3 * i17], _mm_sub_pd(_mm_mul_pd(r17, r15), _mm_mul_pd(r17, r16)));
            r3D[3 * i17 + 2] = b_a_tmp * sphiTCP0[3 * i17 + 2] - b_a_tmp * cphiTEcrCP0[3 * i17 + 2];
        }
    } else {
        binary_expand_op(r3D, b_a_tmp, sphiTCP0, b_a_tmp, cphiTEcrCP0);
    }
}

//
// function [ r0D, r1D, r2D, r3D ] = EvalHelix( CurvStruct, u_vec, maskCart )
//
// EvalHelix : Evalue the helix curv and its corresponding parametric
//  derivatives. The evaluation occurs on the specified points in the u
//  vector.
//
//  Inputs :
//  CurvStruct    : A struct filled the parameters correspondin to a Helix
//  u_vec         : A vector of specifided points for the evaluation of the
//                  curve
//  maskCart      : Carthesian mask. It indicates which index should be
//                considerate as carthesian one.
//
//  Outputs :
//  r0D           : The evaluated helix at the specified points
//  r1D           : The 1rst order parametric derivative of the curve at the
//                  specified points
//  r2D           : The 2nd order parametric derivative of the curve at the
//                  specified points
//  r3D           : The 3rd order parametric derivative of the curve at the
//                  specified points
//
// Arguments    : const ::coder::array<double, 1U> &CurvStruct_R0
//                const double CurvStruct_CorrectedHelixCenter[3]
//                const double CurvStruct_evec[3]
//                double CurvStruct_theta
//                double CurvStruct_pitch
//                const double u_vec[2]
//                const bool maskCart_data[]
//                const int maskCart_size[2]
//                double r0D[2][3]
//                double r1D[2][3]
//                double r2D[2][3]
//                double r3D[2][3]
// Return Type  : void
//
void d_EvalHelix(const ::coder::array<double, 1U> &CurvStruct_R0,
                 const double CurvStruct_CorrectedHelixCenter[3], const double CurvStruct_evec[3],
                 double CurvStruct_theta, double CurvStruct_pitch, const double u_vec[2],
                 const bool maskCart_data[], const int maskCart_size[2], double r0D[2][3],
                 double r1D[2][3], double r2D[2][3], double r3D[2][3])
{
    __m128d r1;
    __m128d r10;
    __m128d r11;
    __m128d r12;
    __m128d r13;
    __m128d r14;
    __m128d r15;
    __m128d r16;
    __m128d r17;
    __m128d r18;
    __m128d r19;
    __m128d r2;
    __m128d r20;
    __m128d r21;
    __m128d r3;
    __m128d r4;
    __m128d r5;
    __m128d r6;
    __m128d r7;
    __m128d r8;
    __m128d r9;
    double P0_data[6];
    double cphiTCP0[2][3];
    double cphiTEcrCP0[2][3];
    double sphiTCP0[2][3];
    double sphiTEcrCP0[2][3];
    double CP0[3];
    double EcrCP0[3];
    double y[3];
    double a;
    double a_tmp;
    double b_a;
    double b_a_tmp;
    double d;
    double d1;
    double d2;
    double d3;
    double d4;
    double r0D_tmp;
    int end;
    int partialTrueCount;
    int trueCount;
    signed char tmp_data[6];
    // 'EvalHelix:22' if ~coder.target('MATLAB')
    // 'EvalHelix:23' coder.cinclude('common/tracy/Tracy.hpp');
    // 'EvalHelix:24' coder.inline('never')
    // 'EvalHelix:25' coder.ceval('ZoneScopedN', coder.opaque('const char*', '"EvalHelix"'));
    ZoneScopedN("EvalHelix");
    //  Extract parameters from the struct
    // 'EvalHelix:28' P0      = CurvStruct.R0( maskCart );
    end = maskCart_size[1] - 1;
    trueCount = 0;
    partialTrueCount = 0;
    for (int i{0}; i <= end; i++) {
        if (maskCart_data[i]) {
            trueCount++;
            tmp_data[partialTrueCount] = static_cast<signed char>(i + 1);
            partialTrueCount++;
        }
    }
    for (int b_i{0}; b_i < trueCount; b_i++) {
        P0_data[b_i] = CurvStruct_R0[tmp_data[b_i] - 1];
    }
    // 'EvalHelix:29' P1      = CurvStruct.R1( maskCart );
    // 'EvalHelix:30' evec    = CurvStruct.evec;
    // 'EvalHelix:31' theta   = CurvStruct.theta;
    // 'EvalHelix:32' pitch   = CurvStruct.pitch;
    // 'EvalHelix:34' P0P1    = P0 - P1;
    //
    // 'EvalHelix:37' C           = CurvStruct.CorrectedHelixCenter;
    // 'EvalHelix:38' CP0         = P0 - C;
    if (trueCount == 3) {
        __m128d r;
        r = _mm_loadu_pd(&P0_data[0]);
        _mm_storeu_pd(
            &CP0[0],
            _mm_sub_pd(r, _mm_loadu_pd((const double *)&CurvStruct_CorrectedHelixCenter[0])));
        CP0[2] = P0_data[2] - CurvStruct_CorrectedHelixCenter[2];
    } else {
        minus(CP0, P0_data, &trueCount, CurvStruct_CorrectedHelixCenter);
    }
    // 'EvalHelix:39' phi_vec     = theta * u_vec;
    // 'EvalHelix:40' EcrCP0      = cross( evec, CP0 );
    EcrCP0[0] = CurvStruct_evec[1] * CP0[2] - CP0[1] * CurvStruct_evec[2];
    EcrCP0[1] = CP0[0] * CurvStruct_evec[2] - CurvStruct_evec[0] * CP0[2];
    EcrCP0[2] = CurvStruct_evec[0] * CP0[1] - CP0[0] * CurvStruct_evec[1];
    //  clockwise tangent vector
    // 'EvalHelix:41' cphi        = mycos( phi_vec );
    //  mycos : My implementation of cosinus.
    //
    //  Inputs :
    //    x   : The value use to evaluate the cosinus.
    //  Outputs :
    //    y   : The resulting value of the cosinus.
    //
    // 'mycos:10' y = cos( x );
    // 'mycos:11' cos_calls = cos_calls + 1;
    cos_calls++;
    // 'EvalHelix:42' sphi        = mysin( phi_vec );
    //  mysin : Custom implementation of the sinus.
    //
    //  Inputs :
    //  Inputs values of the function.
    //
    //  Outputs :
    //  Resulting values.
    //
    // 'mysin:11' y = sin( x );
    // 'mysin:12' sin_calls = sin_calls + 1;
    sin_calls++;
    //
    // 'EvalHelix:45' cphiTCP0    = CP0 * cphi;
    // 'EvalHelix:46' sphiTCP0    = CP0 * sphi;
    // 'EvalHelix:47' cphiTEcrCP0 = EcrCP0 * cphi;
    // 'EvalHelix:48' sphiTEcrCP0 = EcrCP0 * sphi;
    //
    // 'EvalHelix:51' r0D       = bsxfun(@plus, C, cphiTCP0  + sphiTEcrCP0  + ...
    // 'EvalHelix:52'                    pitch/(2*pi)*evec*phi_vec);
    a = CurvStruct_pitch / 6.2831853071795862;
    d = CurvStruct_theta * u_vec[0];
    d1 = std::cos(d);
    d2 = std::sin(d);
    r1 = _mm_loadu_pd(&CP0[0]);
    r3 = _mm_set1_pd(d1);
    r2 = _mm_mul_pd(r1, r3);
    _mm_storeu_pd(&cphiTCP0[0][0], r2);
    r4 = _mm_set1_pd(d2);
    _mm_storeu_pd(&sphiTCP0[0][0], _mm_mul_pd(r1, r4));
    r5 = _mm_loadu_pd(&EcrCP0[0]);
    _mm_storeu_pd(&cphiTEcrCP0[0][0], _mm_mul_pd(r5, r3));
    r6 = _mm_mul_pd(r5, r4);
    _mm_storeu_pd(&sphiTEcrCP0[0][0], r6);
    r7 = _mm_mul_pd(_mm_set1_pd(a), _mm_loadu_pd((const double *)&CurvStruct_evec[0]));
    r8 = _mm_set1_pd(0.0);
    r9 = _mm_loadu_pd((const double *)&CurvStruct_CorrectedHelixCenter[0]);
    _mm_storeu_pd(&r0D[0][0],
                  _mm_add_pd(r9, _mm_add_pd(_mm_add_pd(r2, r6),
                                            _mm_add_pd(r8, _mm_mul_pd(r7, _mm_set1_pd(d))))));
    d3 = CP0[2] * d1;
    cphiTCP0[0][2] = d3;
    sphiTCP0[0][2] = CP0[2] * d2;
    cphiTEcrCP0[0][2] = EcrCP0[2] * d1;
    d4 = EcrCP0[2] * d2;
    sphiTEcrCP0[0][2] = d4;
    r0D_tmp = a * CurvStruct_evec[2];
    r0D[0][2] = CurvStruct_CorrectedHelixCenter[2] + ((d3 + d4) + r0D_tmp * d);
    d = CurvStruct_theta * u_vec[1];
    d1 = std::cos(d);
    d2 = std::sin(d);
    r1 = _mm_loadu_pd(&CP0[0]);
    r10 = _mm_set1_pd(d1);
    r2 = _mm_mul_pd(r1, r10);
    _mm_storeu_pd(&cphiTCP0[1][0], r2);
    r11 = _mm_set1_pd(d2);
    _mm_storeu_pd(&sphiTCP0[1][0], _mm_mul_pd(r1, r11));
    r5 = _mm_loadu_pd(&EcrCP0[0]);
    _mm_storeu_pd(&cphiTEcrCP0[1][0], _mm_mul_pd(r5, r10));
    r6 = _mm_mul_pd(r5, r11);
    _mm_storeu_pd(&sphiTEcrCP0[1][0], r6);
    _mm_storeu_pd(&r0D[1][0],
                  _mm_add_pd(r9, _mm_add_pd(_mm_add_pd(r2, r6),
                                            _mm_add_pd(r8, _mm_mul_pd(r7, _mm_set1_pd(d))))));
    d3 = CP0[2] * d1;
    cphiTCP0[1][2] = d3;
    sphiTCP0[1][2] = CP0[2] * d2;
    cphiTEcrCP0[1][2] = EcrCP0[2] * d1;
    d4 = EcrCP0[2] * d2;
    sphiTEcrCP0[1][2] = d4;
    r0D[1][2] = CurvStruct_CorrectedHelixCenter[2] + ((d3 + d4) + r0D_tmp * d);
    // 'EvalHelix:53' r1D       = bsxfun(@plus, -theta  *sphiTCP0  + theta  *cphiTEcrCP0, ...
    // 'EvalHelix:54'                    theta * pitch/(2*pi) * evec);
    b_a = CurvStruct_theta * CurvStruct_pitch / 6.2831853071795862;
    _mm_storeu_pd(&y[0],
                  _mm_mul_pd(_mm_set1_pd(b_a), _mm_loadu_pd((const double *)&CurvStruct_evec[0])));
    y[2] = b_a * CurvStruct_evec[2];
    // 'EvalHelix:55' r2D       = -theta^2*cphiTCP0  - theta^2*sphiTEcrCP0;
    a_tmp = CurvStruct_theta * CurvStruct_theta;
    // 'EvalHelix:56' r3D       =  theta^3*sphiTCP0  - theta^3*cphiTEcrCP0;
    b_a_tmp = std::pow(CurvStruct_theta, 3.0);
    r12 = _mm_loadu_pd(&sphiTCP0[0][0]);
    r13 = _mm_loadu_pd(&cphiTEcrCP0[0][0]);
    r14 = _mm_loadu_pd(&y[0]);
    r15 = _mm_set1_pd(-CurvStruct_theta);
    r16 = _mm_set1_pd(CurvStruct_theta);
    _mm_storeu_pd(&r1D[0][0],
                  _mm_add_pd(_mm_add_pd(_mm_mul_pd(r15, r12), _mm_mul_pd(r16, r13)), r14));
    r17 = _mm_loadu_pd(&cphiTCP0[0][0]);
    r18 = _mm_loadu_pd(&sphiTEcrCP0[0][0]);
    r19 = _mm_set1_pd(-a_tmp);
    r20 = _mm_set1_pd(a_tmp);
    _mm_storeu_pd(&r2D[0][0], _mm_sub_pd(_mm_mul_pd(r19, r17), _mm_mul_pd(r20, r18)));
    r21 = _mm_set1_pd(b_a_tmp);
    _mm_storeu_pd(&r3D[0][0], _mm_sub_pd(_mm_mul_pd(r21, r12), _mm_mul_pd(r21, r13)));
    r1D[0][2] = (-CurvStruct_theta * sphiTCP0[0][2] + CurvStruct_theta * cphiTEcrCP0[0][2]) + y[2];
    r2D[0][2] = -a_tmp * cphiTCP0[0][2] - a_tmp * sphiTEcrCP0[0][2];
    r3D[0][2] = b_a_tmp * sphiTCP0[0][2] - b_a_tmp * cphiTEcrCP0[0][2];
    r12 = _mm_loadu_pd(&sphiTCP0[1][0]);
    r13 = _mm_loadu_pd(&cphiTEcrCP0[1][0]);
    r14 = _mm_loadu_pd(&y[0]);
    _mm_storeu_pd(&r1D[1][0],
                  _mm_add_pd(_mm_add_pd(_mm_mul_pd(r15, r12), _mm_mul_pd(r16, r13)), r14));
    r17 = _mm_loadu_pd(&cphiTCP0[1][0]);
    r18 = _mm_loadu_pd(&sphiTEcrCP0[1][0]);
    _mm_storeu_pd(&r2D[1][0], _mm_sub_pd(_mm_mul_pd(r19, r17), _mm_mul_pd(r20, r18)));
    _mm_storeu_pd(&r3D[1][0], _mm_sub_pd(_mm_mul_pd(r21, r12), _mm_mul_pd(r21, r13)));
    r1D[1][2] = (-CurvStruct_theta * sphiTCP0[1][2] + CurvStruct_theta * cphiTEcrCP0[1][2]) + y[2];
    r2D[1][2] = -a_tmp * d3 - a_tmp * d4;
    r3D[1][2] = b_a_tmp * sphiTCP0[1][2] - b_a_tmp * cphiTEcrCP0[1][2];
}

} // namespace ocn

//
// File trailer for EvalHelix.cpp
//
// [EOF]
//
