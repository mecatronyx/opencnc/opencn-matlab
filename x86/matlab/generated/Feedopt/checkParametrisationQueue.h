
//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: checkParametrisationQueue.h
//
// MATLAB Coder version            : 5.4
//

#ifndef CHECKPARAMETRISATIONQUEUE_H
#define CHECKPARAMETRISATIONQUEUE_H

// Include Files
#include "rtwtypes.h"
#include <cstddef>
#include <cstdlib>

// Custom Header Code
#include "functions.h"
// Type Declarations
namespace ocn {
class queue_coder;

}

// Function Declarations
namespace ocn {
bool checkParametrisationQueue(const queue_coder *queue);

}

#endif
//
// File trailer for checkParametrisationQueue.h
//
// [EOF]
//
