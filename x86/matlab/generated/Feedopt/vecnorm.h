
//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: vecnorm.h
//
// MATLAB Coder version            : 5.4
//

#ifndef VECNORM_H
#define VECNORM_H

// Include Files
#include "rtwtypes.h"
#include "coder_array.h"
#include <cstddef>
#include <cstdlib>

// Custom Header Code
#include "functions.h"
// Function Declarations
namespace ocn {
namespace coder {
void vecnorm(const ::coder::array<double, 2U> &x, ::coder::array<double, 2U> &y);

}
} // namespace ocn

#endif
//
// File trailer for vecnorm.h
//
// [EOF]
//
