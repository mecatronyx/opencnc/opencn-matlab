
//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: anglesNormDin.h
//
// MATLAB Coder version            : 5.4
//

#ifndef ANGLESNORMDIN_H
#define ANGLESNORMDIN_H

// Include Files
#include "rtwtypes.h"
#include <cstddef>
#include <cstdlib>

// Custom Header Code
#include "functions.h"
// Function Declarations
namespace ocn {
extern double anglesNormDin(double A_prev, double A, int A_mode, bool G90);

}

#endif
//
// File trailer for anglesNormDin.h
//
// [EOF]
//
