
//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: unsafeSxfun.h
//
// MATLAB Coder version            : 5.4
//

#ifndef UNSAFESXFUN_H
#define UNSAFESXFUN_H

// Include Files
#include "rtwtypes.h"
#include "coder_array.h"
#include <cstddef>
#include <cstdlib>

// Custom Header Code
#include "functions.h"
// Function Declarations
namespace ocn {
void binary_expand_op(::coder::array<double, 1U> &in1, const ::coder::array<double, 1U> &in3,
                      double in4, const ::coder::array<double, 1U> &in5, double in6, double in7);

}

#endif
//
// File trailer for unsafeSxfun.h
//
// [EOF]
//
