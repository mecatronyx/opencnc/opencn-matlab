
//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: diag.h
//
// MATLAB Coder version            : 5.4
//

#ifndef DIAG_H
#define DIAG_H

// Include Files
#include "rtwtypes.h"
#include "coder_array.h"
#include <cstddef>
#include <cstdlib>

// Custom Header Code
#include "functions.h"
// Function Declarations
namespace ocn {
namespace coder {
void diag(const ::coder::array<double, 2U> &v, ::coder::array<double, 1U> &d);

}
} // namespace ocn

#endif
//
// File trailer for diag.h
//
// [EOF]
//
