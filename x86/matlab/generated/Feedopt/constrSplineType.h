
//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: constrSplineType.h
//
// MATLAB Coder version            : 5.4
//

#ifndef CONSTRSPLINETYPE_H
#define CONSTRSPLINETYPE_H

// Include Files
#include "opencn_matlab_types1.h"
#include "rtwtypes.h"
#include <cstddef>
#include <cstdlib>

// Custom Header Code
#include "functions.h"
// Function Declarations
namespace ocn {
extern void constrSplineType(double a__1, SplineStruct *C);

}

#endif
//
// File trailer for constrSplineType.h
//
// [EOF]
//
