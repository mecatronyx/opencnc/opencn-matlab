
//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: diff.h
//
// MATLAB Coder version            : 5.4
//

#ifndef DIFF_H
#define DIFF_H

// Include Files
#include "rtwtypes.h"
#include "coder_array.h"
#include <cstddef>
#include <cstdlib>

// Custom Header Code
#include "functions.h"
// Function Declarations
namespace ocn {
namespace coder {
void diff(const ::coder::array<double, 2U> &x, ::coder::array<double, 2U> &y);

}
} // namespace ocn

#endif
//
// File trailer for diff.h
//
// [EOF]
//
