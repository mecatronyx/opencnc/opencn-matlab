
//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: constrCurvStructType.h
//
// MATLAB Coder version            : 5.4
//

#ifndef CONSTRCURVSTRUCTTYPE_H
#define CONSTRCURVSTRUCTTYPE_H

// Include Files
#include "opencn_matlab_types111.h"
#include "rtwtypes.h"
#include <cstddef>
#include <cstdlib>

// Custom Header Code
#include "functions.h"
// Function Declarations
namespace ocn {
extern void constrCurvStructType(double a__1, CurvStruct *C);

}

#endif
//
// File trailer for constrCurvStructType.h
//
// [EOF]
//
