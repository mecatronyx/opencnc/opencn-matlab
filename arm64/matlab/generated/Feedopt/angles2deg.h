
//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: angles2deg.h
//
// MATLAB Coder version            : 5.4
//

#ifndef ANGLES2DEG_H
#define ANGLES2DEG_H

// Include Files
#include "rtwtypes.h"
#include <cstddef>
#include <cstdlib>

// Custom Header Code
#include "functions.h"
// Function Declarations
namespace ocn {
extern void angles2deg(double angles[3]);

}

#endif
//
// File trailer for angles2deg.h
//
// [EOF]
//
