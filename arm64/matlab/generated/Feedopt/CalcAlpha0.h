
//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: CalcAlpha0.h
//
// MATLAB Coder version            : 5.4
//

#ifndef CALCALPHA0_H
#define CALCALPHA0_H

// Include Files
#include "rtwtypes.h"
#include "coder_array.h"
#include <cstddef>
#include <cstdlib>

// Custom Header Code
#include "functions.h"
// Function Declarations
namespace ocn {
void binary_expand_op(double in1_data[], int *in1_size, const double in2[16],
                      const double in3_data[], const int *in3_size,
                      const ::coder::array<double, 1U> &in4, const ::coder::array<double, 1U> &in5);

}

#endif
//
// File trailer for CalcAlpha0.h
//
// [EOF]
//
