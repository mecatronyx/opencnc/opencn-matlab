
//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: ResampleStateClass.cpp
//
// MATLAB Coder version            : 5.4
//

// Include Files
#include "ResampleStateClass.h"

// Function Definitions
//
// Arguments    : void
// Return Type  : ResampleStateClass
//
namespace ocn {
ResampleStateClass::ResampleStateClass() = default;

//
// Arguments    : void
// Return Type  : void
//
ResampleStateClass::~ResampleStateClass() = default;

} // namespace ocn

//
// File trailer for ResampleStateClass.cpp
//
// [EOF]
//
