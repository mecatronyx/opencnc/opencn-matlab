
//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: constrGcodeInfoStructType.h
//
// MATLAB Coder version            : 5.4
//

#ifndef CONSTRGCODEINFOSTRUCTTYPE_H
#define CONSTRGCODEINFOSTRUCTTYPE_H

// Include Files
#include "opencn_matlab_types2.h"
#include "rtwtypes.h"
#include <cstddef>
#include <cstdlib>

// Custom Header Code
#include "functions.h"
// Function Declarations
namespace ocn {
extern void constrGcodeInfoStructType(double a__1, GcodeInfoStruct *C);

}

#endif
//
// File trailer for constrGcodeInfoStructType.h
//
// [EOF]
//
