
//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: bspline_copy.h
//
// MATLAB Coder version            : 5.4
//

#ifndef BSPLINE_COPY_H
#define BSPLINE_COPY_H

// Include Files
#include "opencn_matlab_types2.h"
#include "rtwtypes.h"
#include <cstddef>
#include <cstdlib>

// Custom Header Code
#include "functions.h"
// Function Declarations
namespace ocn {
extern void bspline_copy(const BaseSplineStruct *Bl, BaseSplineStruct *copy);

}

#endif
//
// File trailer for bspline_copy.h
//
// [EOF]
//
