
//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: constrToolStructType.h
//
// MATLAB Coder version            : 5.4
//

#ifndef CONSTRTOOLSTRUCTTYPE_H
#define CONSTRTOOLSTRUCTTYPE_H

// Include Files
#include "opencn_matlab_types11.h"
#include "rtwtypes.h"
#include <cstddef>
#include <cstdlib>

// Custom Header Code
#include "functions.h"
// Function Declarations
namespace ocn {
extern void constrToolStructType(double a__1, Tool *C);

}

#endif
//
// File trailer for constrToolStructType.h
//
// [EOF]
//
