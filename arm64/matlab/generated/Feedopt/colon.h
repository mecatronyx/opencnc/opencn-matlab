
//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: colon.h
//
// MATLAB Coder version            : 5.4
//

#ifndef COLON_H
#define COLON_H

// Include Files
#include "rtwtypes.h"
#include "coder_array.h"
#include <cstddef>
#include <cstdlib>

// Custom Header Code
#include "functions.h"
// Function Declarations
namespace ocn {
namespace coder {
void eml_integer_colon_dispatcher(int b, ::coder::array<int, 2U> &y);

}
} // namespace ocn

#endif
//
// File trailer for colon.h
//
// [EOF]
//
