
//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: checkGeometry.h
//
// MATLAB Coder version            : 5.4
//

#ifndef CHECKGEOMETRY_H
#define CHECKGEOMETRY_H

// Include Files
#include "rtwtypes.h"
#include <cstddef>
#include <cstdlib>

// Custom Header Code
#include "functions.h"
// Type Declarations
namespace ocn {
class queue_coder;

}

// Function Declarations
namespace ocn {
bool checkGeometry(const queue_coder *queueCurv);

}

#endif
//
// File trailer for checkGeometry.h
//
// [EOF]
//
