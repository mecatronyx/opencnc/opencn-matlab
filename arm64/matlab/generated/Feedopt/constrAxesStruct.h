
//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: constrAxesStruct.h
//
// MATLAB Coder version            : 5.4
//

#ifndef CONSTRAXESSTRUCT_H
#define CONSTRAXESSTRUCT_H

// Include Files
#include "opencn_matlab_types3.h"
#include "rtwtypes.h"
#include <cstddef>
#include <cstdlib>

// Custom Header Code
#include "functions.h"
// Function Declarations
namespace ocn {
extern void constrAxesStruct(double x, double y, double z, double a, double b, double c, double u,
                             double v, double w, Axes *CStruct);

}

#endif
//
// File trailer for constrAxesStruct.h
//
// [EOF]
//
