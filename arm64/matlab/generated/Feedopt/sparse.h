
//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: sparse.h
//
// MATLAB Coder version            : 5.4
//

#ifndef SPARSE_H
#define SPARSE_H

// Include Files
#include "rtwtypes.h"
#include "coder_array.h"
#include <cstddef>
#include <cstdlib>

// Custom Header Code
#include "functions.h"
// Type Declarations
namespace ocn {
namespace coder {
class sparse;

}
} // namespace ocn

// Function Declarations
namespace ocn {
namespace coder {
void b_sparse(const ::coder::array<double, 2U> &varargin_1, sparse *y);

}
} // namespace ocn

#endif
//
// File trailer for sparse.h
//
// [EOF]
//
