
//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: opencn_matlab_data.h
//
// MATLAB Coder version            : 5.4
//

#ifndef OPENCN_MATLAB_DATA_H
#define OPENCN_MATLAB_DATA_H

// Include Files
#include "opencn_matlab_internal_types.h"
#include "opencn_matlab_types.h"
#include "opencn_matlab_types11.h"
#include "rtwtypes.h"
#include <cstddef>
#include <cstdlib>

// Custom Header Code
#include "functions.h"
// Variable Declarations
namespace ocn {
extern double cos_calls;
extern double sin_calls;
extern double DebugConfig;
extern const char cv1[2048];
extern const char cv2[30];
extern const char cv3[30];
extern bool isInitialized_opencn_matlab;
} // namespace ocn

#endif
//
// File trailer for opencn_matlab_data.h
//
// [EOF]
//
