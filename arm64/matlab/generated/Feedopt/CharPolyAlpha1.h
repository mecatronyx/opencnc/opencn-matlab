
//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: CharPolyAlpha1.h
//
// MATLAB Coder version            : 5.4
//

#ifndef CHARPOLYALPHA1_H
#define CHARPOLYALPHA1_H

// Include Files
#include "rtwtypes.h"
#include <cstddef>
#include <cstdlib>

// Custom Header Code
#include "functions.h"
// Function Declarations
namespace ocn {
void CharPolyAlpha1(const double in1[16], double Coeff_Poly_Alpha1[10]);

}

#endif
//
// File trailer for CharPolyAlpha1.h
//
// [EOF]
//
