
//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: opencn_matlab_terminate.h
//
// MATLAB Coder version            : 5.4
//

#ifndef OPENCN_MATLAB_TERMINATE_H
#define OPENCN_MATLAB_TERMINATE_H

// Include Files
#include "rtwtypes.h"
#include <cstddef>
#include <cstdlib>

// Custom Header Code
#include "functions.h"
// Function Declarations
namespace ocn {
extern void opencn_matlab_terminate();

}

#endif
//
// File trailer for opencn_matlab_terminate.h
//
// [EOF]
//
