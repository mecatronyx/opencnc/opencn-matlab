
//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: CopyCurvStruct.h
//
// MATLAB Coder version            : 5.4
//

#ifndef COPYCURVSTRUCT_H
#define COPYCURVSTRUCT_H

// Include Files
#include "opencn_matlab_types111.h"
#include "rtwtypes.h"
#include <cstddef>
#include <cstdlib>

// Custom Header Code
#include "functions.h"
// Function Declarations
namespace ocn {
extern void CopyCurvStruct(const CurvStruct *curve, CurvStruct *copy);

}

#endif
//
// File trailer for CopyCurvStruct.h
//
// [EOF]
//
