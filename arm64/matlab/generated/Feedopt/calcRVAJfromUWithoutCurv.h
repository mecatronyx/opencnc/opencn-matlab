
//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: calcRVAJfromUWithoutCurv.h
//
// MATLAB Coder version            : 5.4
//

#ifndef CALCRVAJFROMUWITHOUTCURV_H
#define CALCRVAJFROMUWITHOUTCURV_H

// Include Files
#include "rtwtypes.h"
#include "coder_array.h"
#include <cstddef>
#include <cstdlib>

// Custom Header Code
#include "functions.h"
// Function Declarations
namespace ocn {
void b_times(::coder::array<double, 2U> &in1, const ::coder::array<double, 2U> &in2,
             const ::coder::array<double, 2U> &in3);

void binary_expand_op(::coder::array<double, 2U> &in1, const ::coder::array<double, 2U> &in2,
                      const ::coder::array<double, 2U> &in3, const ::coder::array<double, 2U> &in4,
                      const ::coder::array<double, 2U> &in5);

void binary_expand_op(::coder::array<double, 2U> &in1, const ::coder::array<double, 2U> &in2,
                      const ::coder::array<double, 2U> &in3, const ::coder::array<double, 2U> &in4,
                      const ::coder::array<double, 2U> &in5, const ::coder::array<double, 2U> &in6,
                      const ::coder::array<double, 2U> &in7, const ::coder::array<double, 2U> &in8);

} // namespace ocn

#endif
//
// File trailer for calcRVAJfromUWithoutCurv.h
//
// [EOF]
//
