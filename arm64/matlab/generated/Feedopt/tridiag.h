
//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: tridiag.h
//
// MATLAB Coder version            : 5.4
//

#ifndef TRIDIAG_H
#define TRIDIAG_H

// Include Files
#include "rtwtypes.h"
#include "coder_array.h"
#include <cstddef>
#include <cstdlib>

// Custom Header Code
#include "functions.h"
// Function Declarations
namespace ocn {
void binary_expand_op(::coder::array<double, 2U> &in1, int in2,
                      const ::coder::array<double, 2U> &in3, const ::coder::array<double, 1U> &in4,
                      double in5);

}

#endif
//
// File trailer for tridiag.h
//
// [EOF]
//
