
//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: bsxfun.h
//
// MATLAB Coder version            : 5.4
//

#ifndef BSXFUN_H
#define BSXFUN_H

// Include Files
#include "rtwtypes.h"
#include "coder_array.h"
#include <cstddef>
#include <cstdlib>

// Custom Header Code
#include "functions.h"
// Function Declarations
namespace ocn {
namespace coder {
void bsxfun(const ::coder::array<double, 2U> &a, const double b[5], ::coder::array<double, 2U> &c);

}
} // namespace ocn

#endif
//
// File trailer for bsxfun.h
//
// [EOF]
//
