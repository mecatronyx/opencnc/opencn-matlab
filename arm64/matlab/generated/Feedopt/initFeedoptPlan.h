
//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
// File: initFeedoptPlan.h
//
// MATLAB Coder version            : 5.4
//

#ifndef INITFEEDOPTPLAN_H
#define INITFEEDOPTPLAN_H

// Include Files
#include "opencn_matlab_types.h"
#include "rtwtypes.h"
#include <cstddef>
#include <cstdlib>

// Custom Header Code
#include "functions.h"
// Function Declarations
namespace ocn {
extern void initFeedoptPlan(const FeedoptConfig cfg, FeedoptContext *ctx);

}

#endif
//
// File trailer for initFeedoptPlan.h
//
// [EOF]
//
