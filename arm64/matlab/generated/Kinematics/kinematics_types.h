
/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: kinematics_types.h
 *
 * MATLAB Coder version            : 5.4
 */

#ifndef KINEMATICS_TYPES_H
#define KINEMATICS_TYPES_H

/* Include Files */
#include "rtwtypes.h"

/* Custom Header Code */
#include "functions.h"
#endif
/*
 * File trailer for kinematics_types.h
 *
 * [EOF]
 */
