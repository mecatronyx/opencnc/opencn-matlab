#pragma once
#include <matlab_headers.h>

/**
 * @brief OpenCN builtin print msg.
 * 
 * @param ocn_msg 
 */
void ocn_print_msg( ocn::MsgStruct* ocn_msg );