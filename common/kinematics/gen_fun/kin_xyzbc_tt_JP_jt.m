function JP_jt = kin_xyzbc_tt_JP_jt(in1,in2,in3)
%kin_xyzbc_tt_JP_jt
%    JP_jt = kin_xyzbc_tt_JP_jt(IN1,IN2,IN3)

%    This function was generated by the Symbolic Math Toolbox version 9.1.
%    07-Nov-2023 14:54:35

%Jacobian first derivative ( Inverse Kinematics ) : 
%INPUTS : 
%	r_t    (5x1) : pose vector ( piece frame )
%	v_t    (5x1) : speed vector ( piece frame )
%	a_t    (5x1) : acceleration vector( piece frame )
%	p      (3x5) : parameters
%OUTPUTS : 
%	M      (5x5) : resulting matrix
%#codegen
b = in1(4,:);
c = in1(5,:);
offB1 = in3(10);
offC1 = in3(13);
offB3 = in3(12);
offC2 = in3(14);
offC3 = in3(15);
offP1 = in3(7);
offP2 = in3(8);
offP3 = in3(9);
tp1 = in2(1,:);
tp2 = in2(2,:);
tp3 = in2(3,:);
tp4 = in2(4,:);
tp5 = in2(5,:);
x = in1(1,:);
y = in1(2,:);
z = in1(3,:);
t2 = cos(b);
t3 = cos(c);
t4 = sin(b);
t5 = sin(c);
t6 = offP1+x;
t7 = offP2+y;
t8 = offP3+z;
t9 = offC1.*t2.*t3;
t10 = offC2.*t2.*t3;
t11 = offC1.*t2.*t5;
t12 = offC1.*t3.*t4;
t13 = offC2.*t2.*t5;
t14 = offC2.*t3.*t4;
t15 = offC1.*t4.*t5;
t16 = offC2.*t4.*t5;
t17 = t2.*t3.*t6;
t18 = t2.*t3.*t7;
t19 = t2.*t5.*t6;
t20 = t3.*t4.*t6;
t21 = t2.*t5.*t7;
t22 = t3.*t4.*t7;
t23 = t4.*t5.*t6;
t24 = t4.*t5.*t7;
t25 = -t13;
t26 = -t21;
t27 = t10+t11+t18+t19;
t28 = t14+t15+t22+t23;
mt1 = [-t3.*t4.*tp4-t2.*t5.*tp5,t3.*tp5,-t2.*t3.*tp4+t4.*t5.*tp5,0.0,0.0,-t2.*t3.*tp5+t4.*t5.*tp4,-t5.*tp5,t2.*t5.*tp4+t3.*t4.*tp5,0.0,0.0,t2.*tp4,0.0,-t4.*tp4,0.0,0.0,-tp4.*(t9+t17+t25+t26+offB1.*t2+offB3.*t4+offC3.*t4+t4.*t8)+t2.*tp3+t28.*tp5-t3.*t4.*tp1+t4.*t5.*tp2,0.0,-t4.*tp3+t27.*tp5-tp4.*(-t12+t16-t20+t24-offB1.*t4+offB3.*t2+offC3.*t2+t2.*t8)-t2.*t3.*tp1+t2.*t5.*tp2,0.0,0.0,-tp5.*(t9+t17+t25+t26)+t28.*tp4-t2.*t3.*tp2-t2.*t5.*tp1];
mt2 = [-tp5.*(offC2.*t3+offC1.*t5+t3.*t7+t5.*t6)+t3.*tp1-t5.*tp2,t27.*tp4+tp5.*(t12-t16+t20-t24)+t3.*t4.*tp2+t4.*t5.*tp1,0.0,0.0];
JP_jt = reshape([mt1,mt2],5,5);
