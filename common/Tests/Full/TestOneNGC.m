function ctx = TestOneNGC(cfg)
ctx = InitFeedoptPlan(cfg);
ctx = FeedoptPlanRun(ctx);
end