%% Basic example
% This is script provides a rapid overview of the different steps required
% by the algorithm.
%
clc; clear all; close all;

check_wkdir(); % If current directory is the working directory

sourceFileName = 'ngc_test/E2C/037_dome_gravure_v1.ngc';

% Flag for reusing previous context
LoadContextFromFile             = false; % Load ctx from previous run
SaveContextInFile               = false; % Save ctx in file for further analysis   
PlotGeometry                    = true; % Print the geometry curves
CreateFileForSampler            = false; % Create file for the sampler
DeleteCSVFileAfterResampling    = false; % Delete CSV file after resampling

if( coder.target( "MATLAB" ) )
    % Enable debuging objects
    DebugCusp.getInstance.reset;
    DebugCompressing.getInstance.reset;
    DebugTransition.getInstance.reset;
    DebugOptimization.getInstance.reset;
    DebugResampling.getInstance.reset;
end

if( LoadContextFromFile )
    [ ret, ctx ] = loadCtx( "dev/test.mat" );
end

if( ~exist( 'ctx', 'var' ) )
    % Load default configuration parameters
    cfg = FeedoptDefaultConfig;
    ConfigSetSource( cfg, sourceFileName );
      
    % Set the path to the gcode file
    cfg.source = sourceFileName;
    
    % Initialization of the feed operator
    ctx = initFeedoptPlan( cfg );
end

% Logging
setupLogs( cfg.LogFileName ); diary on;

try
    % Run the geometrics operations, then solve the LP problem
    ctx = FeedoptPlanRun( ctx );
    if( ctx.errcode ~= FeedoptPlanError.NoError )
        error( '%s\n', ctx.errmsg.msg( 1 : ctx.errmsg.size ) );
    end

    if( SaveContextInFile )
        [ ret ] = saveCtx( ctx, "dev/test.mat" );
    end

    if( coder.target( 'MATLAB' ) )
        DebugOptimization.getInstance().print();
    end
    
    if( PlotGeometry )
        [ fig ] = plotGeometry( ctx, ctx.q_gcode, ctx.q_spline, ...
            "Gcode Line" );
        [ fig ] = plotGeometryOnlyStartPoints( ctx, ctx.q_gcode, ...
            ctx.q_spline, "Gcode Point", fig );
        [ fig ] = plotGeometry( ctx, ctx.q_compress, ctx.q_spline, ...
            "Compressing", fig );
        messagePrompt();
        pause( 0.5 );
    end


    % Resampling of the parameter
    if( CreateFileForSampler )
        fileName = "PR2/sampler/dome.csv";
        resample4sampler( ctx, fileName );
    end

    %Resampling of the parameter
    if( ctx.q_opt.isempty ), error("Queue empty before resampling"); end
    fileName = '.tmp.csv' ;
    resample2file( ctx, fileName ); ctx.q_opt.delete();

    if( coder.target( "MATLAB" ) )
        DebugResampling.getInstance.print();
    end

    % Load resampled data points
    res = readmatrix( fileName );    
    
    if( DeleteCSVFileAfterResampling )
        delete( fileName );
    end
    % Transforms structure into vector for analysis
    [res_struct, indFeed, indAcc, indJerk, indRPiece] = get_res_struct( ...
        res, ctx.cfg.maskTot );

    % Analyse time optimality and constraints satisfaction
    analyse_optimality( res, indFeed, indAcc, indJerk, ctx.cfg.dt );

    % Plot the resulting trajectories
    plotTrajectories( ctx, res_struct );

    % Plot the resulting axis commands
    plotAxisCommands( ctx, res_struct );

    % Save run data and plot in folder
    saveResults( sourceFileName );

catch ME
    DestroyContext(ctx);
    error( '%s\n%s\n%s\n', ME.message, "File name : " + ME.stack(1).name, ...
                           "Line : " + ME.stack(1).line );
end

% Free external memory (see queue function)
DestroyContext(ctx);

diary off;


%-------------------------------------------------------------------------%
%% Utility Functions
%-------------------------------------------------------------------------%

function [status] = check_constraints( tol, feed, a, j )
status = 0;
% max constraints respect verif
if ( feed > 1 + tol.v_tol ), bitset(status, 1); end

if any( a > 1 + tol.a_tol ), bitset(status, 2); end

if any( j > 1 + tol.j_tol ), bitset(status, 3); end

end

function [t_opt] = check_time_optimality( tol, feed, a, j )
N = length( feed );
t_opt = zeros(N, 4);

% max constraints respect verif
t_opt( : ,1 ) = ( feed > 1 - tol.tol_opt_v );

t_opt( : ,2 ) = any( a > 1 - tol.tol_opt_a, 2);

t_opt( :, 3 ) = any( j > 1 - tol.tol_opt_j, 2);

t_opt( :, 4 ) = any( t_opt( :, 1:3 ) ,2 );

end

function [] = analyse_optimality( res, indFeed, indAcc, indJerk, dt )
% Load parameters for validation
PfileName = pwd + "/Validate_OpenCN/params.m";
run(PfileName);

tol.v_tol       = vmax_norm_tol;
tol.a_tol       = amax_xyz_tol;
tol.j_tol       = jmax_xyz_tol;
tol.tol_opt_v   = tol_opt_vnorm;
tol.tol_opt_a   = tol_opt_a;
tol.tol_opt_j   = tol_opt_j;
tol.TOpt_tol    = TOpt_tol;

status     = check_constraints( tol, res( :, indFeed ), ...
                                     res( :, indAcc ), ...
                                     res( :, indJerk ) );
disp( optSolStatus2String( status ) );

t_opt_res  = check_time_optimality( tol, res( :, indFeed ), ...
                                         res( :, indAcc ), ...
                                         res( :, indJerk ) );

disp("Machining time : " + res(end, 1) * dt);

disp("Optimality : "                + sum( t_opt_res(:, end) ) / ...
    res(end, 1) * 100 + "[%]" );

disp("Optimality feedrate : "       + sum( t_opt_res(:, 1) ) / ...
    res(end, 1) * 100 + "[%]" );
disp("Optimality acceleration : "   + sum( t_opt_res(:, 2) ) / ...
    res(end, 1) * 100 + "[%]" );
disp("Optimality jerk : "           + sum( t_opt_res(:, 3) ) / ...
    res(end, 1) * 100 + "[%]" );
end

function [res_struct, indFeed, indAcc, indJerk, indRPiece] = ...
    get_res_struct( res, mask )
ind         = 1 : sum( mask );
indFeed     = 3;
indR        = 5                 + ind;
indAcc      = indR( end )       + ind;
indJerk     = indAcc( end )     + ind;
indRPiece   = indJerk( end )    + ind;

res_struct.tvec       = res( :, 1 );
res_struct.uvec       = res( :, 2 );
res_struct.vvec       = res( :, 3 ) .* res( :, 4 ) * 60;
res_struct.fvec       = res( :, 4 ) * 60;
res_struct.cfvec      = res( :, 5 ) * 60;
res_struct.pvec       = res( :, indR );
res_struct.avec       = res( :, indAcc );
res_struct.jvec       = res( :, indJerk );
res_struct.rpiece     = res( :, indRPiece );

end

function [msg] = optSolStatus2String(status)
% optSolStatus2String :
% Translate the returned status of the optimization into its corresponding
% message
%
% status : Status of the returned solution.
%
% msg    : Message corresponding to a given status

msg = "Optimization successed !";

constr = bitand(status, 7);
if constr ~= 0
    if bitget(constr, 1)
        msg = 'V max exceeded!.\n';
    elseif bitget(constr, 2)
        msg = 'A max exceeded!.\n';
    elseif bitget(constr, 3)
        msg = 'J max exceeded!.\n';
    end
end

if bitand(status, 8) ~= 0
    msg = 'Not time optimal!\n';
end
end

function [] = setupLogs( logFileName )

mkdir logs;

diary ([logFileName, '_', ...
    datestr(now,'yyyy_mm_dd_HH_MM_SS'), ...
    '.txt']);

% debug config init
global DebugConfig
DebugConfig = 0;

EnableDebugLog(DebugCfg.OptimProgress);
EnableDebugLog(DebugCfg.Validate);
EnableDebugLog(DebugCfg.FeedratePlanning);
EnableDebugLog(DebugCfg.Error);
EnableDebugLog(DebugCfg.Plots);
end

function plotAxisCommands( ctx, res_struct )
    N = double( ctx.cfg.NumberAxis );
    time = res_struct.tvec;
    axisNameGen = {"x", "y", "z", "a", "b", "c"};
    axisUnitGen = {"mm", "mm", "mm", "rad", "rad", "rad"};
    axisName    = axisNameGen(1, ctx.cfg.maskTot);
    axisUnit    = axisUnitGen(1, ctx.cfg.maskTot);

    figure("Name","Axis Commands : Position");
    for j = 1 : N
        subplot(N, 1, j);
        plot(time, res_struct.pvec(:, j)); grid on;
        ylabel( axisName{ j } + " in " + axisUnit{ j } ); 
    end
    xlabel("Time in [s]");

    plot_name = "Axis Commands : Computed feedrate"; figure("Name",plot_name);
    
    plot(time, res_struct.cfvec, 'r', 'LineWidth', 3); hold on;
    plot(time, res_struct.fvec, 'b'); grid on;
    title( plot_name );
    ylabel( "Feedrate in [mm/s]" ); xlabel( "Time in [s]" );
    legend( "Feedrate target", "Actual Feedrate");

    v       = diff(res_struct.pvec) / ctx.cfg.dt;
    v_norm  = v ./ ctx.cfg.vmax( ctx.cfg.maskTot );
    
    plot_name = "Axis Commands : Normalized Velocity"; figure("Name",plot_name);

    for j = 1 : N
        subplot(N, 1, j);
        plot(time(2:end), v_norm(:,j)); grid on;
        ylabel( "v_" + axisName{ j } + " in " + axisUnit{ j } + "/s");
        if( j == 1 ), title( plot_name ), end
    end

    xlabel("Time in [s]");
    a       = diff(v) / ctx.cfg.dt;
    a_norm  = a ./ ctx.cfg.amax( ctx.cfg.maskTot );

    plot_name = "Axis Commands : Normalized Acceleration"; figure("Name",plot_name);
    for j = 1 : N
        subplot(N, 1, j);
        plot(time(3:end), a_norm(:,j)); grid on;
        ylabel( "a_" + axisName{ j } + " in " + axisUnit{ j } + "/s^2");
        if( j == 1 ), title( plot_name ), end
    end
    xlabel("Time in [s]");

    jerk    = diff(a) / ctx.cfg.dt;
    j_norm  = jerk ./ ctx.cfg.jmax( ctx.cfg.maskTot );

    plot_name = "Axis Commands : Normalized Jerk"; figure("Name",plot_name);
    for j = 1 : N
        subplot(N, 1, j);
        plot(time(4:end), j_norm(:,j)); grid on;
        ylabel( "j_" + axisName{ j } + " in " + axisUnit{ j } + "/s^3");
        if( j == 1 ), title( plot_name ), end
    end
    xlabel("Time in [s]");
end
