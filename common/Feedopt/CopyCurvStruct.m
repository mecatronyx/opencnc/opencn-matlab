function [ copy ] = CopyCurvStruct( curve )
%#codegen
% CopyCurvStruct : Copy the curve structure
%
% Inputs :
% Curv  : The curve to copy
%
% Outputs :
% Copy  : The copy of the curve
%
    copy = curve;
end
