function [ angles ] = angles2rad( angles )
% angles2deg : Rad to degree conversion on a vector of angles.
% 
% Inputs :
% angles : Vector of angles expressed in degree
%
% Outputs:
% angles : Vector of angles expressed in rad
    angles = deg2rad( angles );
end