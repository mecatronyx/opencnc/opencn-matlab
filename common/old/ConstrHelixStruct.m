function CurvStruct = ConstrHelixStruct(trafo, HSC, ...
                      Poff, Aoff, Uoff, Doff, P0, P1, A0, A1, U0, U1, ...
                      Cprim, delta, evec, theta, pitch, FeedRate, mode)
%#codegen
CoeffP5     = zeros(3, 6);

CurvStruct  = ConstrCurvStruct(CurveType.Helix, mode, trafo, HSC, ...
                                Poff, Aoff, Uoff, Doff, P0, ...
                                P1, A0, A1, U0, U1, Cprim, delta, evec,...
                                theta, pitch, CoeffP5, FeedRate);



coder.cstructname(CurvStruct, 'CurvStruct');
